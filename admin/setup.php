<?php
include ("../php/defined.php");
  $db = new PDO("mysql:host=$db_host;dbname=$db_db", $db_user, $db_pass);
  $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $cmd="create table list ( id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,img VARCHAR(255),name VARCHAR(100), family VARCHAR(100), birthdate VARCHAR(100), job VARCHAR(100), jobtitle VARCHAR(100), city VARCHAR(100), lang VARCHAR(100), address VARCHAR(512), carid VARCHAR(100), number VARCHAR(100), telegram VARCHAR(100), instagram VARCHAR(100), socialid VARCHAR(255), extra TEXT , hash VARCHAR(255) ,h_counter INT(11) DEFAULT 1 ,ref_id BIGINT DEFAULT 0 ,reported BOOLEAN DEFAULT false, time TIMESTAMP )";
  $comment_table_patern="create table comments(comment_number BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY ,ref_id bigint not null , username varchar(255) NOT NULL , comment text not null , time TIMESTAMP ) ";
  $username_table_pattern="create table users (username varchar(255) , hashpass varchar(255) , register_time TIMESTAMP )";
  $harassment_pattern="create table harassment(id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY , ref_id BIGINT , hash VARCHAR(255) ,time TIMESTAMP , reported BOOLEAN DEFAULT false ,category TINYINT , description TEXT , country VARCHAR (3) , province VARCHAR(255) , city INT , age TINYINT , image_file VARCHAR(255) , audio_file VARCHAR(255) , video_file VARCHAR(255) , zip_file VARCHAR(255) ,report_to_boss VARCHAR(1) ,partner_support VARCHAR(1) ,familly_support VARCHAR(1) ,friend_support VARCHAR(1) ,report_to_police VARCHAR(1) ,rap_sheet VARCHAR(1) ,punished VARCHAR(1) ,keep_on VARCHAR(1) , police_raiting VARCHAR(1) ,therapy VARCHAR(1) , why TEXT )"; # we cant use INT(1) instead of VARCHAR(1), because a null string "" in int convert to '0' that means 'NO' in html form and not unselected. 
  try {
  $db->query($cmd);
  $db->query($comment_table_patern);
  $db->query($username_table_pattern);
  $db->query($harassment_pattern);
} catch (Exception $ex) {
    echo $ex->getmessage();
      
}
