<?php 
session_start();
require("./defined.php");
$err_invalid_id="<span style='color:red' dir=rtl > اطلاعات درست به ما نرسیدند-احتمال دستکاری اطلاعات توسط فرد سوم! </span>";
$length_of_hash_string=60;
$username=test_input_raw($_POST['username']); # username must be 4 to 10 char
$pass=test_input_raw($_POST['pass']);
$comment=test_input_raw($_POST['comment']);
if ( empty ($comment) or $comment == ' ') {exit ("نظرتان را ننوشتید");}

if ( ! $ref_id=test_id( $_POST['id'] ) ) { exit($err_invalid_id);}
$username_length=mb_strlen($username,'utf-8');
$pass_length=mb_strlen($pass,'utf-8');
if($username_length < 4 || $username_length > 10){
exit("<span style='color:red;' dir=rtl >طول نام کاربری باید بین ۵ تا ۱۰ خرف  باشد</span>");}
if($pass_length < 6 || $pass_length > 30){
exit("<span style='color:red;' dir=rtl >طول کلمه عبور باید بین ۶ تا ۳۰ حرف  باشد</span>");}
try{
$db= new PDO("mysql:host=$db_host;dbname=$db_db" , $db_user, $db_pass );
$patern_get_user_info="SELECT hashpass from users WHERE username like :username";
$stmt= $db->prepare($patern_get_user_info);
$stmt->execute(["username"=>$username]);
// echo sizeof($stmt->fetchAll(PDO::FETCH_COLUMN,0));
$hashpass=$stmt->fetchAll(PDO::FETCH_COLUMN,0)[0];
$err_invalid_usr_pass="<span style='color:red;' dir=rtl >نام کاربری یا پسورد اشتباه است.</span>";
if (strlen($hashpass) != $length_of_hash_string ){exit($err_invalid_usr_pass);}
if (! password_verify($pass , $hashpass)) { exit ($err_invalid_usr_pass);}
$patern_comment_last_time="SELECT time FROM comments WHERE username LIKE :username order BY time DESC LIMIT 1";
$stmt=$db->prepare($patern_comment_last_time);
$stmt->execute(["username"=>$username]);
date_default_timezone_set('UTC');
$user_last_comment_time=strtotime($stmt->fetchAll(PDO::FETCH_COLUMN,0)[0]);
$current_time=time();
if ( $current_time - $user_last_comment_time < 600 ) { exit("فاصله زمانی بین ارسال دو نظر حداقل باید ۱۰ دقیه باشد");}
$patern_comment_add="INSERT INTO comments (ref_id , username , comment ) VALUES ( :ref_id , :username , :comment)";
$stmt=$db->prepare($patern_comment_add);
$stmt->execute(['ref_id'=>$ref_id , 'username'=>$username , 'comment' => $comment]);
echo "<span style='color:green;'> نظر شما اضافه شد! </span>";
}
catch(PDOException $e)
    {
    echo  $e->getMessage();
    }
?>
