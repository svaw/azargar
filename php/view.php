          <?php
          $html_css_file='<link rel="stylesheet" type="text/css" href="../css/view.css">';
          require ("./defined.php");
          include ("./message_fa.php");
          echo $html_header;
            $db = new PDO("mysql:host=$db_host;dbname=$db_db", $db_user , $db_pass);
            $my_url="http://".$_SERVER['HTTP_HOST'] .$_SERVER['REQUEST_URI'];
          
        if ( ! $id=test_id( $_GET['id'] )){exit("id error");}
        $stmt = $db->prepare("select ref_id , reported from list where id = :id");
        $stmt->execute(['id'=>$id]);
        $stmt->bindcolumn('ref_id' , $ref_id);
        $stmt->bindcolumn('reported' , $reported);
        $stmt->fetch(PDO::FETCH_BOUND);
        $add_info_btn=true;
        $is_this_main_page=true;
        $comment_window_enable=true;
        if ( $ref_id != "0" ){ # 0 is string! all return from database is strng
        echo "<h1 class=fa_text style='text-align:center'> توجه در این صفحه اطلاعات تکمیلی قرار دارد <br> <a href=view.php?id=$ref_id> تمام اطلاعات مربوط به این فرد</a></h1>";
        $add_info_btn=false; # extra info page does not have add_info_btn!
        $is_this_main_page=false; #this is  for general purpose. 
        $comment_window_enable=false;
        }
        $name=$family=$birthdate=$job=$jobtitle=$city=$lang=$address=$carid=$number=$telegram=$instagram=$socialid=$extra="";#this is for prevent get warning (PHP Notice:  Undefined variable)
        #this is  begining of history section 
        $h_table="<div ><a  id=history_state href=# onclick=\"history('hide');return false;\" > بستن لیست تاریخچه </a><br><table id=history>
        <tr>
<th > تصویر </th>
<th > نام </th>
<th> نام خانوادگی </th>
<th> تاریخ تولد </th>
<th> شغل </th>
<th> رتبه سازمانی </th>
<th> شهر </th>
<th> زبان/لهجه </th>
<th> آدرس </th>
<th> پلاک خودرو </th>
<th> تلفن/موبایل </th>
<th> id تلگرام </th>
<th> Id اینستاگرام </th>
<th> Idهای دیگر </th>
<th> توضیحات </th>
<th>ویرایش </th>
<th> حذف </th>
    </tr>";
    $stmt = $db->prepare("select id from list where id = :id OR ref_id = :id ");
    $stmt->execute(['id' => $id ]);
    $history_list=$stmt->fetchAll(PDO::FETCH_COLUMN, 0);
//     if (sizeof ($history_list) == 0 ){
//     $history_list=array($id);}
    $stmt = $db->prepare($patern_output_by_id);
//     $stmt->bindColumn('id' , $h_id); #h_id=$history_item
    $stmt->bindColumn('img' , $h_img); #h_ means relative to history table
    $stmt->bindColumn('name' , $h_name);
    $stmt->bindColumn('family' , $h_family);
    $stmt->bindColumn('birthdate' , $h_birthdate);
    $stmt->bindColumn('job' , $h_job);
    $stmt->bindColumn('jobtitle' , $h_jobtitle);
    $stmt->bindColumn('city' , $h_city);
    $stmt->bindColumn('lang' , $h_lang);
    $stmt->bindColumn('address' , $h_address);
    $stmt->bindColumn('carid' , $h_carid);
    $stmt->bindColumn('number' , $h_number);
    $stmt->bindColumn('telegram' , $h_telegram);
    $stmt->bindColumn('instagram' , $h_instagram);
    $stmt->bindColumn('socialid' , $h_socialid);
    $stmt->bindColumn('extra' , $h_extra);
    $stmt->bindColumn('h_counter' , $h_h_counter); #h_counter means harassement counter! dont conflict with history prefix (h_).
    $img_list="";
    foreach( $history_list as $history_item){
    $stmt->execute(['id' => $history_item]);
    $stmt->fetch(PDO::FETCH_BOUND);
$name .= " $h_name";
$family .= " $h_family";
$birthdate .= " $h_birthdate";
$job .= " $h_job";
$jobtitle .= " $h_jobtitle";
$city .= " $h_city";
$lang .= " $h_lang";
$address .= " $h_address";
$carid .= " $h_carid";
$number .= " $h_number";
$telegram .= " $h_telegram";
$instagram .= " $h_instagram";
$socialid .= " $h_socialid";
$extra .= " $h_extra";
if ($h_img != "../img/no-img.png"){
$img_list .= "<img src='../$faces_dir/$h_img'style='display:block;width:100%;margin-top=10vw;' >";
}

    $h_counter += intval ($h_h_counter); #h_counter in add-info sub page always is 0 so its not do anythhng but its prevent overwrite h_counter in the loop!

    $h_table .= "<tr>
<td> <img id='img' src='../$faces_dir/$h_img' /></td>
<td> $h_name </td>
<td> $h_family </td>
<td> ".num_to_fa($h_birthdate)." </td>
<td> $h_job </td>
<td> $h_jobtitle </td>
<td> $h_city </td>
<td> $h_lang </td>
<td> ".num_to_fa($h_address)." </td>
<td> ".num_to_fa($h_carid)." </td>
<td> ".num_to_fa($h_number)." </td>
<td> $h_telegram </td>
<td> $h_instagram </td>
<td> $h_socialid </td>
<td> ".num_to_fa($h_extra)." </td>
<td> <a href=edit.php?id=$history_item> ویرایش </a> </td>
<td> <a href=delete.php?id=$history_item>حذف </a> </td>
          </tr>";
           } #foreach end
           $h_table .= "</table><br></div>";
           if ($add_info_btn){
           $h_table .= "<a class=fa_text style='display:block;clear:both;font-size: 1.5em;' href=add_info.php?id=$id>{افزودن اطلاعات تکمیلی}</a>";}
           #end history section
        
        $report_note="<p><h1><a  id=report_note href=# onclick=\"report('$id');return false;\" > درخواست بازبینی این صفحه </a></h1><center id=report_info >فقط اشکالات تایپی و اطلاعات نامربوط بررسی می شود مثلا عکس بلبل به جای آزارگر </center></p>";
        if ( intval ($reported) == "1" ) {
        $report_note=$reported_page_msg; }
        if ( $img_list == ""){ $img_list="<img src='../img/no-img.png'style='display:block;width:100%;margin-top=10vw;' >";}
        $comment_window=""; # this is for prevent get warning 
        if($comment_window_enable){
        include ("embeded_comment.php");
        }
        echo "
            $report_note
            $h_table
            <div class='content'>
            $img_list</div>
                <div class='content fa_text' >
                 <p><b> نام : </b> $name</p>
<p><b> نام خانوادگی : </b> $family</p>
<p><b> تاریخ تولد : </b>".num_to_fa($birthdate)."</p>
<p><b> شغل : </b> $job</p>
<p><b> رتبه سازمانی : </b> $jobtitle</p>
<p><b> شهر : </b> $city</p>
<p><b> زبان/لهجه : </b> $lang</p>
<p><b> آدرس : </b> ".num_to_fa($address)."</p>
<p><b> پلاک خودرو : </b> ".num_to_fa($carid)."</p>
<p><b> تلفن/موبایل : </b> ".num_to_fa($number)."</p>
<p><b> Id تلگرام : </b> $telegram</p>
<p><b> Id اینستاگرام : </b> $instagram</p>
<p><b> Idهای دیگر : </b> $socialid</p>
<p><b> توضیحات : </b> ".num_to_fa($extra)."</p>
<p><b> تعداد افراد آزاردیده:  </b><span id=h_counter> ". num_to_fa( $h_counter)." </span>
<a  id=counter_plus_btn href=# onclick=\"me_too('$id');return false;\" > [من را هم آزار داده] </a>
</p>
    
    <hr width=75% align=center >
    <center>
    <a href='https://www.facebook.com/sharer.php?u=$my_url'><img class='sharelink' src=../img/facebook.png /></img></a>
    <a href='https://twitter.com/intent/tweet?url=$my_url'><img class='sharelink' src=../img/twitter.png /><img /></a>
    <a href='https://telegram.me/share/url?url=$my_url'><img class='sharelink' src=../img/tel.png /><img /></a>
    <a href='$my_url'><img class='sharelink' src=../img/web.png /><img /></a>
$comment_window 
    </center>
</div>
<script src='../js/view.js'></script>
<script src='../js/comment.js'></script>
</div> 
$html_footer";
    
    
    ?>
        
     
