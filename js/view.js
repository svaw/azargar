function me_too(id) {
  var counter_obj = new XMLHttpRequest();
  if(counter_obj){
  counter_obj.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("h_counter").innerHTML = this.responseText;
      document.getElementById("counter_plus_btn").style.color = "green";
      document.getElementById("counter_plus_btn").innerHTML = "گذارش شما ثبت شد";
      document.getElementById("counter_plus_btn").removeAttribute("href");
      document.getElementById("counter_plus_btn").removeAttribute("onclick");
    }
  };
  counter_obj.open("GET", "ajax_counter.php?id="+id +'&'+'foo='+ Math.random(), true);
  counter_obj.send();
}
}
function report(id) {
  var report_obj = new XMLHttpRequest();
  if(report_obj){
  report_obj.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      if (  this.responseText == "success" ){
      document.getElementById("report_note").style.color = "red";
      document.getElementById("report_note").innerHTML = ".این صفحه جهت بازبینی علامت گذاری شد";
      document.getElementById("report_note").removeAttribute("href");
      document.getElementById("report_note").removeAttribute("onclick");
      document.getElementById("report_info").style.display="none";
    }
    }
  };
  report_obj.open("GET", "ajax_report.php?id="+id +'&'+'foo='+ Math.random(), true);
  report_obj.send();
}
}
function history(status){
    if (status == "show"){
        document.getElementById("history").style.display="block";
        document.getElementById("history_state").innerHTML="بستن لیست تاریخچه";
        document.getElementById("history_state").onclick=function (){history('hide');return false;};
    }
    if (status == "hide"){
        document.getElementById("history").style.display="none";
        document.getElementById("history_state").innerHTML="بازکردن لیست تاریخچه";
        document.getElementById("history_state").onclick=function (){history('show');return false;};
    }
}
