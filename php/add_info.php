<?php 
$html_title="-افزودن اطلاعات جدید";
$html_css_file='<link rel="stylesheet" type="text/css" href="../css/add.css">
    <link rel="stylesheet" type="text/css" media="all" href="../JalaliJSCalendar-1.4/skins/aqua/theme.css" title="Aqua" />
    <link rel="alternate stylesheet" type="text/css" media="all" href="../JalaliJSCalendar-1.4/skins/calendar-blue.css" title="winter" />
    <link rel="stylesheet" type="text/css" href="../css/view.css">';
include ("./defined.php");
echo "$html_header";

if ( ! $id=test_id( $_GET['id'] )){exit;}

$db = new PDO("mysql:host=$db_host;dbname=$db_db", $db_user, $db_pass);

#this is  history section start
        $h_table="<div ><a  id=history_state href=# onclick=\"history('hide');return false;\" > بستن لیست تاریخچه </a><br><table id=history>
        <tr>
<th > تصویر </th>
<th > نام </th>
<th> نام خانوادگی </th>
<th> تاریخ تولد </th>
<th> شغل </th>
<th> رتبه سازمانی </th>
<th> شهر </th>
<th> زبان/لهجه </th>
<th> آدرس </th>
<th> پلاک خودرو </th>
<th> تلفن/موبایل </th>
<th> id تلگرام </th>
<th> Id اینستاگرام </th>
<th> Idهای دیگر </th>
<th> توضیحات </th>
<th>ویرایش </th>
<th> حذف </th>
    </tr>";
    $stmt = $db->prepare("select id from list where ref_id = :id or id = :id");
    $stmt->execute(['id' => $id ]);
    $history_list=$stmt->fetchAll(PDO::FETCH_COLUMN, 0);
    if (sizeof ($history_list) == 0 ){
    $history_list=array($id);}
    $stmt = $db->prepare($patern_output_by_id);
    $stmt->bindColumn('id' , $h_id); #h_ means relative to history table
    $stmt->bindColumn('img' , $h_img);
    $stmt->bindColumn('name' , $h_name);
    $stmt->bindColumn('family' , $h_family);
    $stmt->bindColumn('birthdate' , $h_birthdate);
    $stmt->bindColumn('job' , $h_job);
    $stmt->bindColumn('jobtitle' , $h_jobtitle);
    $stmt->bindColumn('city' , $h_city);
    $stmt->bindColumn('lang' , $h_lang);
    $stmt->bindColumn('address' , $h_address);
    $stmt->bindColumn('carid' , $h_carid);
    $stmt->bindColumn('number' , $h_number);
    $stmt->bindColumn('telegram' , $h_telegram);
    $stmt->bindColumn('instagram' , $h_instagram);
    $stmt->bindColumn('socialid' , $h_socialid);
    $stmt->bindColumn('extra' , $h_extra);
    foreach( $history_list as $history_item){
    $stmt->execute(['id' => $history_item]);
    $stmt->fetch(PDO::FETCH_BOUND);
    $h_table .= "<tr>
<td> <img id='img' src='../$faces_dir/$h_img' /></td>
<td> $h_name </td>
<td> $h_family </td>
<td> ".num_to_fa($h_birthdate)." </td>
<td> $h_job </td>
<td> $h_jobtitle </td>
<td> $h_city </td>
<td> $h_lang </td>
<td> ".num_to_fa($h_address)." </td>
<td> ".num_to_fa($h_carid)." </td>
<td> ".num_to_fa($h_number)." </td>
<td> $h_telegram </td>
<td> $h_instagram </td>
<td> $h_socialid </td>
<td> ".num_to_fa($h_extra)." </td>
<td> <a href=edit.php?id=$id> ویرایش </a> </td>
<td> <a href=delete.php?id=$id>حذف </a> </td>
          </tr>";
          } #foreach end
           $h_table .= "</table></div>";
           #end history section
           echo $h_table;
?>
<div style="clear: both;">
<form action="add_info_confirm.php" method="post" enctype="multipart/form-data">
        <input type="hidden" name="ref_id" value=<?php echo $id; ?> />
        <input type="text" name="name" size="12" placeholder="نام" />
        <input type="text" name="family" size="12" placeholder="نام خانوادگی" /><br/><br/>
        <input name="pic" id="pic" type="file" accept="image/*" onchange="preview(event)" style="display:none" >
        <label id="ilabel" class="formfield" for="pic" style="font-family: 'B Yekan+';">انتخاب تصویر آزارگر</label>
        <img id="im_preview"/>
        <a id="im_rst" onclick="rm_im()">[حذف عکس ] </a><br/><br/>
        <input name="birthdate" id="date_input" size="12" type="text" placeholder="سال یا تاریخ تولد" >
        <img src="../img/cal.jpg" id="date_btn"><br/>
        <input type="text" name="job" size="12" placeholder="شغل" />
        <input type="text" name="jobtitle" size="12" placeholder="رتبه سازمانی" /><br/>
        <input type="text" name="city" size="12" placeholder="شهر" />
        <input type="text" name="lang" size="12" placeholder="لهجه/زبان" /><br/>
        <input type="text" name="address" size="12" placeholder="آدرس"/>
        <input type="text" name="carid" size="12" placeholder="شماره پلاک "/><br/>
        <input type="text" name="number" size="12" placeholder="موبایل یا تلفن"/>
        <input type="text" name="telegram" placeholder="ID تلگرام"/>
        <input type="text" name="instagram" placeholder="ID اینستاگرام"/>
        <input type="text" name="socialid" placeholder="سایت‌ها و شبکه‌های اجتماعی دیگر"/><br>
        <textarea class="formfield" placeholder="توضیحات، شرح اتفاق یا هر اطلاعات دیگری که می خواهید در اینجا ثبت شود" name="extra" ></textarea> <br/>
<img id="captcha" src="../securimage/securimage_show.php" alt="CAPTCHA Image" /><br/>
<a href="#" onclick="document.getElementById('captcha').src = '../securimage/securimage_show.php?' + Math.random(); return false">[ تعویض کد ]</a>
<br>
<input type="text" name="captcha_code" size="10" placeholder="کد عکس" required />
<p class="fa_text" style="display: none;" id="err_box"></p>
<br/><button type="submit">ثبت</button>
</form>
<div>
<?php $html_footer ?>
<script src="../JalaliJSCalendar-1.4/jalali.js"></script>
<script src="../JalaliJSCalendar-1.4/calendar.js"></script>
<script src="../JalaliJSCalendar-1.4/calendar-setup.js"></script>
<script src="../JalaliJSCalendar-1.4/lang/calendar-fa.js"></script>
<script src="../js/add.js"></script>
<script src="../js/view.js"></script>
