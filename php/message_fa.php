<?php 
#general (using in multi page )
$image_value_err="<div class=fa_text><h1>خطا! حجم یا فرمت عکس غیرمجاز است لطفا به <a href='javascript:history.go(-1)'>صفحه قبل </a>برگردید و  دوباره امتحان کنید</h1>$html_footer";
$captcha_code_err="<h1>کد عکس را اشتباه وارد کردید لطفا به <a href='javascript:history.go(-1)'>صفحه قبل </a>برگردید و  دوباره امتحان کنید</h1> $html_footer";
$id_value_err="<span style='color:red' dir=rtl >آی دی صفحه اشتباه است </span>$html_footer";
$db_err=  "<div class='fa_text'><h1> متاسفانه در بانک اطلاعاتی خطایی پیش آمد با ایمیل زیر تماس بگیرید </h1></div>";
$not_permit="شما اجازه انجام این کار را ندارید ";
$session_not_found="شما اجازه انجام این کار را ندارید "; # cockie not found or not supported
$reported_page_msg="<h1 style='color:orange'>این صفحه جهت بازبینی علامت گذاری شده است</h1>";

#delete page section (harasser and harassment)
$delete_page_successfully= "<h1 style='color:green'>:)<br> صفحه با موفقیت حذف شد  </h1>";
$delete_id_invalid_err="<h1 dir=rtl> کد ثبت شما اشتباه است لطفا مطمئن شوید که کد را درست وارد کردید و توجه داشته باشید که شما فقط می توانید اطلاعاتی را که خودتان وارد کردید را حذف ویا ویرایش کنید. می توانید با ما تماس بگیرید. </h1> $html_footer";

#add_harassment_confirm.php 
$description_needed_err="<h1>شما متن آزار را وارد  نکردید </h1> $html_footer";
$category_value_err="<h1>دسته اشتباه است </h1> $html_footer";
$country_value_err="<h1>کد کشور اشتباه است.</h1>";
$province_value_err="<h1>خطا در انتخاب استان </h1> $html_footer";
$city_value_err="<h1>خطا در انتخاب شهر </h1> $html_footer";
$police_raiting_value_err="<h1>خطا در امتیاز دهس به پلیس</h1>$html_footer";
$ref_link_value_err="<h1>لینک به صفحه آزارگر مطابق الگو نیست</h1> $html_footer";
$age_value_err="<h1>سن اشتباه وارد شده است </h1> $html_footer";
$audio_value_err="<div class=fa_text><h1>خطا! حجم یا فرمت فایل صوتی غیرمجاز است لطفا به <a href='javascript:history.go(-1)'>صفحه قبل </a>برگردید و  دوباره امتحان کنید</h1>$html_footer";
$video_value_err="<div class=fa_text><h1>خطا! حجم یا فرمت ویدئو غیرمجاز است لطفا به <a href='javascript:history.go(-1)'>صفحه قبل </a>برگردید و  دوباره امتحان کنید</h1>$html_footer";
$zip_value_err="<div class=fa_text><h1>خطا! حجم یا فرمت فایل zip غیرمجاز است لطفا به <a href='javascript:history.go(-1)'>صفحه قبل </a>برگردید و  دوباره امتحان کنید</h1>$html_footer";

#ajax_othercountry.PHP
$othercountry_value_err="<option value=-1>خطا درانتخاب محل</option>";
$place_not_found_err="<option value=-1 > مکانی وجود ندارد</option>";
$select_stat_item="<option value=0 >استان/ایالت </option>";
$select_city_item="<option value=0 >شهر </option>";

#h_view.php 
function html5_multimedia ($mime , $address ){
return "<$mime src=$address poster=../img/video_poster.jpg preload='none' controls>
<span class=fa_text> متاسفانه مرورگر شما از این فرمت پشتیبانی نمی کند می توانید فایل را
<a href=$address>دانلود</a>کنید</span></$mime>";
}
$malicious_file_war = "<span style=color:red>توجه: هیچ کدام از فایلها توسط ما  از جهت ویروس یا بدافزارها بررسی نمی شود قبل از هرکاری فایلها را  از این بابت بررسی کنید! </span>"; #war is warning 
# *edit.php 
$page_update_successfully="<h1 style='color:green'>:)<br> صفحه با موفقیت بروز شد  </h1>"
?>
