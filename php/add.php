 <?php
session_start();

 $html_title="-ثبت آزارگر جدید";
 require_once ("./defined.php");
 require_once ("./message_fa.php");
 echo $html_header;
 require_once '../securimage/securimage.php';
$securimage = new Securimage();
if ($securimage->check($_POST['captcha_code']) == false) {
exit($captcha_code_err);
}


$name=$family=$birthdate=$job=$jobtitle=$city=$lang=$address=$carid=$number=$telegram=$instagram=$socialid=$extra="";
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $name=test_input($_POST["name"]);
    $family=test_input($_POST["family"]);
    $birthdate=test_input($_POST["birthdate"]);
    $job=test_input($_POST["job"]);
    $jobtitle=test_input($_POST["jobtitle"]);
    $city=test_input($_POST["city"]);
    $lang=test_input($_POST["lang"]);
    $address=test_input($_POST["address"]);
    $carid=test_input($_POST["carid"]);
    $number=test_input($_POST["number"]);
    $telegram=test_input($_POST["telegram"]);
    $instagram=test_input($_POST["instagram"]);
    $socialid=test_input($_POST["socialid"]);
    $extra=test_input($_POST["extra"]);
}

$deleteid=rand() . rand(); # random_int() does not support in php5.
$hash=password_hash($deleteid,PASSWORD_BCRYPT);
$img_file="../img/no-img.png" ;
if ($_FILES['pic']['tmp_name']){
$f_mime=explode('/',mime_content_type($_FILES["pic"]["tmp_name"]))[0];
    if ($f_mime == "image" && ($_FILES['pic']['size'] < $image_max_size )) {
         $img_file=md5_file($_FILES["pic"]["tmp_name"]). "." . pathinfo($_FILES['pic']['name'],PATHINFO_EXTENSION);
        rename($_FILES['pic']['tmp_name'] , "../$faces_dir/$img_file");
    } else {
exit($image_value_err);
    }
}
$check_required_field_less=0;
$hi_check_result = true; //powerful! (form will be valid only by one item)
$less_check_result = true;
$pic="valid";
if ($img_file == "../img/no-img.png") {
    $pic=""; //only used for checking required form field
}
foreach ([$name,$family,$job,$city] as $less_item)
{
    if (!empty($less_item)) {
        $check_required_field_less++;
    }
}
if ( $check_required_field_less < 3 ){
    $less_check_result=false;
}
if ($pic.$address.$carid.$number.$telegram.$instagram.$socialid == ""){
    $hi_check_result = false;
}
if (!($hi_check_result || $less_check_result)){
    exit("<div class='fa_text'><h1>اطلاعاتی که وارد کردید آن قدر عمومی است که ما تمی توانیم آنها را  ثبت کنیم لطفا یه <a href=javascript:history.go(-1) > صفحه قبل </a>   رفته ،
             وستون توضیحات را بخوانید و جزییات بیشتری را وارد کنید.</h1></div><div id='footer' > created with &#10084;</div>  </body></html>");
}
$db = new PDO("mysql:host=$db_host;dbname=$db_db", $db_user, $db_pass);
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$cmd="INSERT INTO list(img,name,family,birthdate,job,jobtitle,city,lang,address,carid,number,telegram,instagram,socialid,extra,hash)
VALUES (:img,:name,:family,:birthdate,:job,:jobtitle,:city,:lang,:address,:carid,:number,:telegram,:instagram,:socialid,:extra,:hash)";
$stmt = $db->prepare($cmd);
$data=['img' => $img_file,
    'name' => $name ,
    'family' => $family ,
    'birthdate' => $birthdate ,
    'job' => $job ,
    'jobtitle' => $jobtitle ,
    'city' => $city ,
    'lang' => $lang ,
    'address' => $address ,
    'carid' => $carid ,
    'number' => $number ,
    'telegram' => $telegram ,
    'instagram' => $instagram ,
    'socialid' => $socialid ,
    'extra' => $extra ,
    'hash' => $hash];
try {
$stmt->execute($data);
$lastid=$db->lastInsertId();
$my_url="$website_base_url/php/view.php?id=$lastid";
echo "<div class='fa_text'><center><h1> شماره ثبت شما: </h1>"
. "<br><h2>$deleteid</h2></br>توجه: شما فقط با این کد می توانید اطلاعاتی را که ثبت کردید را حذف کنید."
        . "<br>"
        ."درصورت تمایل می توانید برای این آزارگر <a href=$website_base_url/add_harassment.php?ref_id=$lastid > یک آزار </a>ثبت کنید.<br>"
        . "لینک دائمی به آزارگر شما:"
        . "<br><a href=$my_url>$my_url</a>"
        . "<br>اشتراک گذاری صفحه آزارگر در شبکه های اجتماعی :<br>"
        . "<a href='https://www.facebook.com/sharer.php?u=$my_url'><img class='sharelink' src=../img/facebook.png /></img></a>
        <a href='https://twitter.com/intent/tweet?url=$my_url'><img class='sharelink' src=../img/twitter.png /><img /></a>
        <a href='https://telegram.me/share/url?url=$my_url'><img class='sharelink' src=../img/tel.png /><img /></a>"
        . "</center></div>";
}
catch(Exception $e) {
//     echo 'Exception -> ';
//    var_dump($e->getMessage());
    echo "<div class='fa_text'><h1> متاسفانه در بانک اطلاعاتی خطایی پیش آمد با ایمیل زیر تماس بگیرید </h1></div>";
}

 echo $html_footer;
 ?>

 
