 <?php 
session_start();
$html_title="-ویرایش";
require ("./config.php");
include ("defined.php");
include ("./message_fa.php");
echo $html_header;
 include_once '../securimage/securimage.php';
$securimage = new Securimage();
if ($securimage->check($_POST['captcha_code']) == false) {
  exit("$captcha_code_err");
}

if ( ! $id=test_id( $_POST['id'] ) or ! $deleteid=test_id( $_POST['key'] ) ){
exit("<h1> اطلاعات به درستی دریافت نشده است. دوباره تلاش کنید یا به ما اطلاع دهید </h1> $html_footer");
}
// ****************get input ******************
$description=test_input($_POST['description']);

$ref_id=0;
if (!empty($_POST['ref_link'])) { 
$ref_link=test_input($_POST['ref_link']);
$url_pattern="|$website_base_url".'/php/view.php\?id=\d(\d*)$|';
if (!preg_match($url_pattern,$ref_link)) {exit($ref_link_value_err);}
$ref_id=intval(explode("=",$ref_link)[1]);
if ($ref_id == 0){exit($ref_link_value_err);}
}

$image_file="undifined" ;
if ($_FILES['image']['tmp_name'] && ! isset($_POST['delete_image'])){
$f_mime=explode('/',mime_content_type($_FILES["image"]["tmp_name"]))[0];
    if ($f_mime == "image" && ($_FILES['image']['size'] < $image_max_size )) {
         $image_file="../$harassment_docs/image/". md5_file($_FILES["image"]["tmp_name"]). "." . pathinfo($_FILES['image']['name'],PATHINFO_EXTENSION);
//         rename($_FILES['image']['tmp_name'] , $image_file);
    } else { exit($image_value_err);}
}

$audio_file="undifined" ;
if ($_FILES['audio']['tmp_name'] && ! isset($_POST['delete_audio'])){
$f_mime=explode('/',mime_content_type($_FILES["audio"]["tmp_name"]))[0];
    if ($f_mime == "audio" && ($_FILES['audio']['size'] < $audio_max_size )) {
         $audio_file="../$harassment_docs/audio/". md5_file($_FILES["audio"]["tmp_name"]). "." . pathinfo($_FILES['audio']['name'],PATHINFO_EXTENSION);
//         rename($_FILES['audio']['tmp_name'] , $audio_file);
    } else { exit($audio_value_err);}
}
$video_file="undifined" ;
if ($_FILES['video']['tmp_name'] && ! isset($_POST['delete_video']) ){
$f_mime=explode('/',mime_content_type($_FILES["video"]["tmp_name"]))[0];
    if ($f_mime == "video" && ($_FILES['video']['size'] < $video_max_size )) {
         $video_file="../$harassment_docs/video/". md5_file($_FILES["video"]["tmp_name"]). "." . pathinfo($_FILES['video']['name'],PATHINFO_EXTENSION);
//         rename($_FILES['video']['tmp_name'] , $video_file);
    } else { exit($video_value_err);}
}
$zip_file="undifined" ;
if ($_FILES['zip']['tmp_name'] && ! isset($_POST['delete_zip']) ){
$f_mime=mime_content_type($_FILES["zip"]["tmp_name"]);
    if ($f_mime == "application/zip" && ($_FILES['zip']['size'] < $zip_max_size )) {
         $zip_file="../$harassment_docs/zip/". md5_file($_FILES["zip"]["tmp_name"]). ".zip" ;
//         rename($_FILES['zip']['tmp_name'] , $zip_file);
    } else { exit($zip_value_err);}
}

$why="";
if(isset($_POST['why'])){$why=test_input($_POST['why']);}


//*****************************************************



$db = new PDO("mysql:host=$db_host;dbname=$db_db", $db_user, $db_pass);
$get_current_file_pattern="SELECT image_file , audio_file , video_file , zip_file FROM harassment WHERE id = :id";
$stmt = $db->prepare($get_current_file_pattern);
$stmt->execute(['id' => $id]);
$stmt->bindcolumn('image_file' , $c_image_file); #c_ for current file value 
$stmt->bindcolumn('audio_file' , $c_audio_file);
$stmt->bindcolumn('video_file' , $c_video_file);
$stmt->bindcolumn('zip_file' , $c_zip_file);
$stmt->fetch(PDO::FETCH_BOUND);

if ( $image_file != "undifined" ) { unlink ("$c_image_file"); rename($_FILES['image']['tmp_name'] , $image_file) ; }
if ( $audio_file != "undifined" ) { unlink ("$c_audio_file"); rename($_FILES['audio']['tmp_name'] , $audio_file) ; }
if ( $video_file != "undifined" ) { unlink ("$c_video_file"); rename($_FILES['video']['tmp_name'] , $video_file) ; }
if ( $zip_file != "undifined" ) { unlink ("$c_zip_file"); rename($_FILES['zip']['tmp_name'] , $zip_file) ; }


if(isset($_POST['delete_image'])) { unlink("$c_image_file" ) ; $image_file = "undifined"; } 
if(isset($_POST['delete_audio'])) { unlink("$c_audio_file" ) ; $audio_file = "undifined"; } 
if(isset($_POST['delete_video'])) { unlink("$c_video_file" ) ; $video_file = "undifined"; } 
if(isset($_POST['delete_zip'])) { unlink("$c_zip_file" ) ; $zip_file = "undifined"; }


// $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$get_hash_patern="SELECT hash FROM harassment WHERE id = :id ";
$stmt = $db->prepare($get_hash_patern);
$stmt->execute(['id' => $id]);
$hash=$stmt->fetchAll(PDO::FETCH_COLUMN, 0)[0];
if (! password_verify($deleteid, $hash)) {
exit ($delete_id_invalid_err);
}

$update_info_patern="UPDATE harassment SET ref_id = :ref_id ,
description = :description,
image_file = :image_file,
audio_file = :audio_file,
video_file = :video_file,
zip_file = :zip_file,
why = :why WHERE id = :id ";
$stmt = $db->prepare($update_info_patern);
$data=["ref_id" => $ref_id ,
"description" => $description ,
"image_file" => $image_file ,
"audio_file" => $audio_file,
"video_file" => $video_file,
"zip_file" => $zip_file,
"why" => $why ,
"id" => $id ];

try {
$stmt->execute($data);
echo $page_update_successfully ;
}
catch(Exception $e) {
    echo $db_err;
//     echo $e->getMessage();
}
echo $html_footer;
 ?>
