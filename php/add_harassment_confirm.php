 <?php
session_start();

 $html_title="-ثبت آزار";
 require ("./config.php");
 require ("./defined.php");
 require ("./message_fa.php");
 echo $html_header;
 require_once '../securimage/securimage.php';
$securimage = new Securimage();
if ($securimage->check($_POST['captcha_code']) == false) {
  exit( "$captcha_code_err");

}
$description=test_input($_POST['description']);
if ($description == "" ) { exit ($description_needed_err);}
$ref_id=0;#its only for prevent get warning: PHP Notice:  Undefined variable (ref_id is BIGINT in DB so default value must be int)
if(!empty($_POST['ref_id'])){if (! $ref_id=test_id($_POST['ref_id'])) {exit($ref_link_value_err);}}

if (!empty($_POST['ref_link'])) { 
$ref_link=test_input($_POST['ref_link']);
$url_pattern="|$website_base_url".'/php/view.php\?id=\d(\d*)$|';
if (!preg_match($url_pattern,$ref_link)) {exit($ref_link_value_err);}
$ref_id=intval(explode("=",$ref_link)[1]);
if ($ref_id == 0){exit($ref_link_value_err);}
}
$category=0;#its only for prevent get warning: PHP Notice:  Undefined variable (category is BIGINT in DB so default value must be int)
if (intval($_POST['category']) != 0 ) { #0 in select menu is unselected option
$category_code_range_a=1;
$category_code_range_b=10; #there is only 10 option
if (! $category=test_num($_POST['category'],$category_code_range_a, $category_code_range_b)) { exit ($category_value_err);}
}
$country="";#its only for prevent get warning: PHP Notice:  Undefined variable
$country=test_input($_POST['country']);
if (!( $_POST['country'] == '0' && $_POST['province'] == "0" && $_POST['city'] == "0")){
if (! preg_match ("/[A-Z][A-Z]/" , $country)) {exit ($country_value_err);}}
if($country=="IR" && $_POST['province'] == "0" && $_POST['city'] == "0"){$country="";}

$province="";#its only for prevent get warning: PHP Notice:  Undefined variable
$province=test_input($_POST['province']);
if ($country == "IR"){
if (intval($_POST['province']) != 0 ) { #0 in select menu is unselected option
$province_code_range_a=1;
$province_code_range_b=30; # there is only 30 province in iran
if ( ! $province=test_num($_POST['province'] , $province_code_range_a , $province_code_range_b)){exit ($province_value_err);} 
} }
$city="0";#its only for prevent get warning: PHP Notice:  Undefined variable
if (intval($_POST['city']) != 0 ) { #0 in select menu is unselected option
$city_code_range_a=1;
$city_code_range_b=48311;# there is only 48311 city in DBase 
if ( $country == "IR") { $city_code_range_b=370; } # there is only 370 city in IRAN 
if ( ! $city=test_num($_POST['city'] , $city_code_range_a , $city_code_range_b)){exit ($city_value_err);} 
}
$age="0";#its only for prevent get warning: PHP Notice:  Undefined variable
if (intval($_POST['age']) != 0 ) { #0 in select menu is unselected option
$age_code_range_a=1;
$age_code_range_b=60;
if ( ! $age=test_num($_POST['age'] , $age_code_range_a , $age_code_range_b)){exit ($age_value_err);} 
}
$police_raiting="";#its only for prevent get warning: PHP Notice:  Undefined variable
if (intval($_POST['police_raiting']) != 0 ) { #0 in select menu is unselected option
$police_raiting_code_range_a=1;
$police_raiting_code_range_b=5;# there is only 5 level
if ( ! $police_raiting=test_num($_POST['police_raiting'] , $police_raiting_code_range_a , $police_raiting_code_range_b)){exit ($police_raiting_value_err);} 
}

$image_file="undifined" ;
if ($_FILES['image']['tmp_name']){
$f_mime=explode('/',mime_content_type($_FILES["image"]["tmp_name"]))[0];
    if ($f_mime == "image" && ($_FILES['image']['size'] < $image_max_size )) {
         $image_file="../$harassment_docs/image/". md5_file($_FILES["image"]["tmp_name"]). "." . pathinfo($_FILES['image']['name'],PATHINFO_EXTENSION);
        rename($_FILES['image']['tmp_name'] , $image_file);
    } else { exit($image_value_err);}
}

$audio_file="undifined" ;
if ($_FILES['audio']['tmp_name']){
$f_mime=explode('/',mime_content_type($_FILES["audio"]["tmp_name"]))[0];
    if ($f_mime == "audio" && ($_FILES['audio']['size'] < $audio_max_size )) {
         $audio_file="../$harassment_docs/audio/". md5_file($_FILES["audio"]["tmp_name"]). "." . pathinfo($_FILES['audio']['name'],PATHINFO_EXTENSION);
        rename($_FILES['audio']['tmp_name'] , $audio_file);
    } else { exit($audio_value_err);}
}
$video_file="undifined" ;
if ($_FILES['video']['tmp_name']){
$f_mime=explode('/',mime_content_type($_FILES["video"]["tmp_name"]))[0];
    if ($f_mime == "video" && ($_FILES['video']['size'] < $video_max_size )) {
         $video_file="../$harassment_docs/video/". md5_file($_FILES["video"]["tmp_name"]). "." . pathinfo($_FILES['video']['name'],PATHINFO_EXTENSION);
        rename($_FILES['video']['tmp_name'] , $video_file);
    } else { exit($video_value_err);}
}
$zip_file="undifined" ;
if ($_FILES['zip']['tmp_name']){
$f_mime=mime_content_type($_FILES["zip"]["tmp_name"]);
    if ($f_mime == "application/zip" && ($_FILES['zip']['size'] < $zip_max_size )) {
         $zip_file="../$harassment_docs/zip/". md5_file($_FILES["zip"]["tmp_name"]). ".zip";
        rename($_FILES['zip']['tmp_name'] , $zip_file);
    } else { exit($zip_value_err);}
}

$report_to_boss=test_checkbox('report_to_boss' , 2 ); # there is two(2) option for checkbox(yes or no!)
$partner_support=test_checkbox('partner_support' , 3 ); #there is three(3) option for checkbox(yes or no or not inform)
$familly_support=test_checkbox('familly_support' , 3 ); 
$friend_support=test_checkbox('friend_support' , 3 ); 
$report_to_police=test_checkbox('report_to_police' , 2 ); 
$rap_sheet=test_checkbox('rap_sheet' , 2 ); 
$punished=test_checkbox('punished' , 2 ); 
$keep_on=test_checkbox('keep_on' , 2 ); 
$therapy=test_checkbox('therapy' , 2 ); 

if(isset($_POST['why'])){$why=test_input($_POST['why']);}
$deleteid=rand() . rand(); # random_int() does not support in php5.
$hash=password_hash($deleteid,PASSWORD_BCRYPT);
$add_harressment_pattern="INSERT INTO harassment ( ref_id  ,hash , category  , description  , country ,province  , city  , age  , image_file  , audio_file  , video_file  , zip_file  ,report_to_boss  ,partner_support  ,familly_support  ,friend_support  ,report_to_police  ,rap_sheet  ,punished  ,keep_on  , police_raiting  ,therapy  , why ) VALUES (:ref_id , :hash , :category , :description , :country , :province , :city , :age , :image_file , :audio_file , :video_file , :zip_file , :report_to_boss , :partner_support , :familly_support , :friend_support , :report_to_police , :rap_sheet , :punished , :keep_on , :police_raiting , :therapy , :why )";
$db = new PDO ("mysql:host=$db_host;dbname=$db_db","$db_user","$db_pass");
// $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
// try {
$stmt = $db->prepare($add_harressment_pattern);
$data = [
'ref_id' => $ref_id ,
'hash' => $hash ,
'category' => $category ,
'description' => $description ,
'country' => $country ,
'province' => $province ,
'city' => $city ,
'age' => $age ,
'image_file' => $image_file ,
'audio_file' => $audio_file ,
'video_file' => $video_file ,
'zip_file' => $zip_file ,
'report_to_boss' => $report_to_boss ,
'partner_support' => $partner_support ,
'familly_support' => $familly_support ,
'friend_support' => $friend_support ,
'report_to_police' => $report_to_police ,
'rap_sheet' => $rap_sheet ,
'punished' => $punished ,
'keep_on' => $keep_on ,
'police_raiting' => $police_raiting ,
'therapy' => $therapy ,
'why' => $why ];
$stmt->execute($data);
$lastid=$db->lastInsertId();
$my_url="$website_base_url/php/h_view.php?id=$lastid";
echo "<div class='fa_text'><center><h1> شماره ثبت شما: </h1>"
. "<br><h2>$deleteid</h2></br>توجه: شما فقط با این کد می توانید اطلاعاتی را که ثبت کردید را حذف کنید."
        . "<br>"
        . "لینک دائمی به صفحه آزار شما:"
        . "<br><a href=$my_url>$my_url</a>"
        . "<br>اشتراک گذاری صفخه آزار شما در شبکه های اجتماعی :<br>"
        . "<a href='https://www.facebook.com/sharer.php?u=$my_url'><img class='sharelink' src=../img/facebook.png /></img></a>
        <a href='https://twitter.com/intent/tweet?url=$my_url'><img class='sharelink' src=../img/twitter.png /><img /></a>
        <a href='https://telegram.me/share/url?url=$my_url'><img class='sharelink' src=../img/tel.png /><img /></a>"
        . "</center></div>";
// }catch (Exception $e) {
//     var_dump( $e->getmessage()); }
echo $html_footer;
?>
