<?php 
$html_css_file="<link rel='stylesheet' type='text/css' href='../css/h_view.css'>";
$html_title="مرور یک آزار";
include("./config.php");
require ("./defined.php");
include("./message_fa.php");
echo $html_header;
if (! $id=test_id($_GET['id'])) {exit($id_value_err);}
$db=new PDO ("mysql:host=$db_host;dbname=$db_db", $db_user , $db_pass);

$stmt=$db->prepare($pattern_h_output_by_id);
$stmt->bindcolumn('ref_id' , $ref_id);
$stmt->bindcolumn('reported' , $reported);
$stmt->bindcolumn('description' , $description);
$stmt->bindcolumn('image_file' , $image_file);
$stmt->bindcolumn('audio_file' , $audio_file);
$stmt->bindcolumn('video_file' , $video_file);
$stmt->bindcolumn('zip_file' , $zip_file);
$stmt->bindcolumn('why' , $why);

$stmt->execute(['id'=>$id]);
if ( $stmt -> rowCount() == 0 ) {exit ($id_value_err);}
$stmt->fetch(PDO::FETCH_BOUND);

$harasser="<span class=fa_text >تاکنون آزارگری برای این  آزار ثبت نشده است</span>";
if (! empty($ref_id) ) { $harasser = "<span class=fa_text><a href=$website_base_url/php/view.php?id=$ref_id>صفحه آزارگر</a></span>";}

$include_file=false;#its TRUE if page has a file and so enable فایل section.
$image_icon=$image="";
if($image_file != "undifined"){ $image="<img style='max-width:100%' src=$image_file alt=image />"; $include_file=true;
$image_icon='<img src=../img/i_mime.png onclick="file_view(\'img\');" />';}
    
$audio_icon=$audio="";
if ($audio_file != "undifined") { $audio = html5_multimedia("audio" , $audio_file); $include_file=true;
$audio_icon='<img src=../img/a_mime.png onclick="file_view(\'aud\');" />';}

$video_icon=$video="";
if ($video_file != "undifined") { $video = html5_multimedia("video" , $video_file);$include_file=true;
$video_icon='<img src=../img/v_mime.png onclick="file_view(\'vid\');" />';}

$zip_icon=$zip="";
if ($zip_file != "undifined"){ $zip = zip_info($zip_file);$include_file=true;
$zip_icon='<img src=../img/z_mime.png onclick="file_view(\'zip\');" />';}

$file_section_header="";
if ($include_file){
$file_section_header="<center><h1 style='margin:0;clear:both;'>فایل های پیوست شده</h1></center>";
}
$desc="";
if (!empty($description)) { $desc = "<b>شرح آزار:</b>$description";}

$why_box="";
if(!empty($why)){$why_box = "<b>چرا آزارگر وجود دارد:</b>$why"; }

?>
<div id=menu >
<a href=h_edit.php?id=<?echo $id;?> class="menu_item" tip="ویرایش صفحه"><img src=../img/edit.png /></a><br>
<a href=h_delete.php?id=<?echo $id;?> class="menu_item" tip="حذف این صفحه" ><img src=../img/del.png /></a>
<a href=# id=report_btn <?php if ($reported) {echo "style='display:none' ";} ?> onclick="report('<?echo $id; ?>');return false;" class="menu_item" tip="درخواست بازبینی این صفحه" ><img src=../img/bug.png /></a>
</div>
<h1 id=report_success ></h1>
<?php if ($reported) {echo $reported_page_msg;} ?>
<div class='page fa_text'><? echo $harasser; ?></div>
<div class='page fa_text'><? echo $desc; ?></div>
<div class='page fa_text'><? echo $why_box ?></div>

<div class='fa_text'><? echo $file_section_header; ?></div>
<center><div style=clear:both;>
    <? echo $image_icon , $audio_icon , $video_icon , $zip_icon; ?>
</div></center>
<center><div id=img class='hide_file'><? echo $image; ?></div>
<div id=aud class='page hide_file'><? echo $audio; ?></div>
<div id=vid class='page hide_file' style="width:100%;"><? echo $video; ?></div>
<div id=zip class='page fa_text hide_file'><? echo $zip; ?></div>
</center>

<script src=../js/h_view.js></script>
<? echo $html_footer; ?>
