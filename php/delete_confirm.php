<?php 
session_start();
$html_title = "-حذف آزار ";
require ("./config.php");
require ("./defined.php");
include ("./message_fa.php");
echo $html_header;
require_once '../securimage/securimage.php';
$securimage = new Securimage();
if ($securimage->check($_POST['captcha_code']) == false) {
  exit( "$captcha_code_err");
}
if ( ! isset(_SESSION['try_to_delete']) ) { exit ($session_not_found);}
if ( ! test_num($_SESSION['try_to_delete'] , 1 , $max_try_to_delete ) ){ $_SESSION['block_time'] = time(); exit ($not_permit);}
$_SESSION['try_to_delete'] += 1;
// $id=$_POST['id'];
// $deleteid=num_to_eng($_POST['key']);
if ( ! $id=test_id( $_POST['id'] ) or ! $deleteid=test_id( $_POST['key'] ) ){ exit; }
$db = new PDO ("mysql:host=$db_host;dbname=$db_db" , $db_user , $db_pass );
$stmt = $db->prepare ("SELECT hash FROM list WHERE id = :id");
$stmt->execute(['id' => $id]);
$hash= $stmt->fetchAll(PDO::FETCH_COLUMN,0)[0];

if (! password_verify($deleteid, $hash)) {
exit ("$delete_id_invalid_err");
}
$stmt = $db->prepare("DELETE FROM list WHERE id = :id");
try {
$stmt->execute(['id' => $id]);
echo "$delete_page_successfully";
}
catch(Exception $e) {
    echo "$db_err";
}
echo $html_footer;
?>
