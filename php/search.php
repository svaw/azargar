<?php   
 $html_title="- جست و جو";
 $html_css_file="<link rel='stylesheet' type='text/css' href='../css/search.css'>";
 include ("./defined.php");
 echo $html_header;
 
 
function err($err_type){
global $html_footer;
switch ( $err_type ) {
    case "no_input":
        exit ("<div class='fa_text'><h1>چیزی برای جست‌و‌جو وارد نکرده‌اید</h1></div>$html_footer");
        break;
    case "not_found":
        echo "<div class='fa_text'><h1>موردی پیدا نشد!  :(</h1></div>";
        break;
    default:
        echo "<div class='fa_text'><h1>خطای پیشبینی نشده</h1><br><h2>لطفا ما را از این خطا اگاه کنید</h2></div>";
        break;
}
}#end function err

$keyword_string=test_input($_POST['keyword']);
if (empty($keyword_string)){
    err("no_input");
    } 
$target=test_input($_POST['target']);
$db = new PDO("mysql:host=$db_host;dbname=$db_db", $db_user, $db_pass );


function search_on_harasser(){
echo "<span class=fa_text style='display:block'>جست و جو در آزارگر ها</span>";
$patern="SELECT id FROM list WHERE 
   name LIKE :keyword
OR city LIKE :keyword
OR family LIKE :keyword
OR birthdate LIKE :keyword
OR job LIKE :keyword
OR jobtitle LIKE :keyword
OR lang LIKE :keyword
OR address LIKE :keyword
OR carid LIKE :keyword
OR number LIKE :keyword
OR telegram LIKE :keyword
OR instagram LIKE :keyword
OR socialid LIKE :keyword
OR extra LIKE :keyword"; 

// try {
global $db,$keyword_string, $patern_output_by_id , $faces_dir;

// $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$stmt = $db->prepare($patern);
$keywords = explode (" " , $keyword_string);
$stmt->execute (['keyword' => "%". $keywords[0] . "%"]);
$result_id = $stmt->fetchAll(PDO::FETCH_COLUMN, 0);
unset($keywords[0]); #remove for skip first keyword because we get it now.

    foreach ($keywords as $keyword ) {
//    print_r($result_id);
    $keyword='%' . $keyword . '%';
    $stmt->execute (['keyword' => $keyword]);
//    echo "$keyword";
    $current_keyword_result = $stmt->fetchAll(PDO::FETCH_COLUMN, 0);
    $result_id = array_intersect($result_id , $current_keyword_result );
    if ( $result_id === [] ) {
    err("not_found");
    return 0 ;
    break;
    }
}
if ( $result_id === [] ) {
err("not_found");
return 0 ;}
//echo "final result ";
$stmt = $db->prepare($patern_output_by_id);
// } #try
// catch(PDOException $e)
//     {
//     echo "Error: " . $e->getMessage(); #it should be disable in release because its lake info about DB!
//     }

//    $stmt->bindColumn('id' , $id);
    $stmt->bindColumn('img' , $img);
    $stmt->bindColumn('name' , $name);
    $stmt->bindColumn('family' , $family);
    $stmt->bindColumn('birthdate' , $birthdate);
    $stmt->bindColumn('job' , $job);
    $stmt->bindColumn('jobtitle' , $jobtitle);
    $stmt->bindColumn('city' , $city);
    $stmt->bindColumn('lang' , $lang);
    $stmt->bindColumn('address' , $address);
    $stmt->bindColumn('carid' , $carid);
    $stmt->bindColumn('number' , $number);
    $stmt->bindColumn('telegram' , $telegram);
    $stmt->bindColumn('instagram' , $instagram);
    $stmt->bindColumn('socialid' , $socialid);
    $stmt->bindColumn('extra' , $extra);
    echo "<table>
    <tr>
<th> تصویر </th>
<th > نام </th>
<th> نام خانوادگی </th>
<th> تاریخ تولد </th>
<th> شغل </th>
<th> رتبه سازمانی </th>
<th> شهر </th>
<th> زبان/لهجه </th>
<th> آدرس </th>
<th> پلاک خودرو </th>
<th> تلفن/موبایل </th>
<th> id تلگرام </th>
<th> Id اینستاگرام </th>
<th> Idهای دیگر </th>
<th> توضیحات </th>
<th></th>
    </tr>";
    foreach ($result_id as $id){
        $stmt->execute(['id' => $id]);
        $stmt->fetch(PDO::FETCH_BOUND);
    echo "<tr>";
    echo "
<td> <img id='img' src='../$faces_dir/$img' /></td>
<td> $name </td>
<td> $family </td>
<td> $birthdate </td>
<td> $job </td>
<td> $jobtitle </td>
<td> $city </td>
<td> $lang </td>
<td> $address </td>
<td> $carid </td>
<td> $number </td>
<td> $telegram </td>
<td> $instagram </td>
<td> $socialid </td>
<td> $extra </td>
<td> <a href=view.php?id=$id>نمایش </a> </td>
          </tr>"  ;}
echo "</table>";
}#end function search_on_harasser

function search_on_harassment(){
echo "<div class=fa_text>جست و جو در آزارها</div>";
global $db,$keyword_string;
$stmt = $db->prepare ("SELECT id FROM harassment WHERE description LIKE :keyword");
$keywords = explode (" " , $keyword_string);
$stmt->execute (['keyword' => "%". $keywords[0] . "%"]);
$result_id = $stmt->fetchAll(PDO::FETCH_COLUMN, 0);
unset($keywords[0]); #remove for skip first keyword because we get it now.

    foreach ($keywords as $keyword ) {
//    print_r($result_id);
    $keyword='%' . $keyword . '%';
    $stmt->execute (['keyword' => $keyword]);
//    echo "$keyword";
    $current_keyword_result = $stmt->fetchAll(PDO::FETCH_COLUMN, 0);
    $result_id = array_intersect($result_id , $current_keyword_result );
    if ( $result_id === [] ) {
    err("not_found");
    return 0 ;
    break;
    }
}
if ( $result_id === [] ) {
err("not_found");
return 0 ;} 
$search_output_from_harassment_db="SELECT ref_id , description FROM harassment WHERE id= :id";
$stmt = $db->prepare($search_output_from_harassment_db);
$stmt->bindColumn('ref_id' , $ref_id);
$stmt->bindColumn('description' , $description);
echo "<table id=harassment_header><tr><th>آیدی آزارگر</th>
<th>شرح آزار</th><th>نمایش</th></tr>";
foreach ($result_id as $id){
$stmt->execute(['id'=>$id]);
$stmt->fetch(PDO::FETCH_BOUND);
if($ref_id == '0') $ref_id="";
echo "<tr><td>$ref_id</td>
<td>$description</td>
<td><a href=h_view.php?id=$id>نمایش </a></td></tr>";
}#end foreach
echo "</table>";
}#end function search_on_harassment

switch($target) {
case "all":
search_on_harasser();
search_on_harassment();
break;
case "harasser":
    search_on_harasser();
    break;
case "harassment";
    search_on_harassment();
    break;
default:
    echo "<span class=fa_text> انتخاب غیرمجاز</span>";
    break;
}

echo  $html_footer;
?>
