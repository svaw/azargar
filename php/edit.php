 
 <?php
$html_title="-ویرایش ";
$html_css_file="<link rel='stylesheet' type='text/css' href='../css/edit.css'>
<link rel='stylesheet' type='text/css' media='all' href='../JalaliJSCalendar-1.4/skins/aqua/theme.css' title='Aqua' />
<link rel='alternate stylesheet' type='text/css' media='all' href='../JalaliJSCalendar-1.4/skins/calendar-blue.css' title='winter' />"; #all html_* must be before include command!
include ( "./defined.php");

if ( ! $id=test_id( $_GET['id'] )){exit;}

$db= new PDO ("mysql:host=$db_host;dbname=$db_db" , $db_user , $db_pass );
$stmt = $db->prepare($patern_output_by_id);
if ( ! $stmt->execute (['id' => $id])) { exit;}

    $stmt->bindColumn('img' , $img);
    $stmt->bindColumn('name' , $name);
    $stmt->bindColumn('family' , $family);
    $stmt->bindColumn('birthdate' , $birthdate);
    $stmt->bindColumn('job' , $job);
    $stmt->bindColumn('jobtitle' , $jobtitle);
    $stmt->bindColumn('city' , $city);
    $stmt->bindColumn('lang' , $lang);
    $stmt->bindColumn('address' , $address);
    $stmt->bindColumn('carid' , $carid);
    $stmt->bindColumn('number' , $number);
    $stmt->bindColumn('telegram' , $telegram);
    $stmt->bindColumn('instagram' , $instagram);
    $stmt->bindColumn('socialid' , $socialid);
    $stmt->bindColumn('extra' , $extra);
$stmt->fetch(PDO::FETCH_BOUND);

$form="<form action='edit_confirm.php' onsubmit='return checkForm()' method='post' enctype='multipart/form-data'>
        <input type='hidden' value=$id name='id' />
        شماره ثبت: <span style='color:red'>*</span>
        <input type='text'  id='key' name='key'  placeholder='شماره ثبت' required /><br>
        نام:
        <input type='text' class='required_field_less' name ='name' size='12' value='$name' />
        نام خانوادگی:
        <input type='text' class='required_field_less' name ='family' size='12' value='$family' /><br/><br/>
        <input name ='pic' id='pic' type='file' accept='image/*' onchange='preview(event)' style='display:none' >
        <label id='ilabel' class='formfield required_field' for='pic' style='font-family: 'B Yekan+';'>انتخاب تصویر آزارگر</label>
        <img id='im_preview' src=$img />
        <a id='im_rst' onclick='rm_im()'>[حذف عکس ] </a><br/><br/>
        تاریخ تولد:
        <input name ='birthdate' id='date_input' size='12' type='text' value='$birthdate' >
        <img src='../img/cal.jpg' id='date_btn'><br/>
        شغل:
        <input type='text' class='required_field_less' name ='job' size='12'  value='$job' />
        رتبه سازمانی:
        <input type='text' name ='jobtitle' size='12' value='$jobtitle' /><br/>
        شهر:
        <input type='text' class='required_field_less' name ='city' size='12' value='$city' />
        زبان/لهجه:
        <input type='text' name ='lang' size='12' value='$lang' /><br/>
        آدرس:
        <input type='text' class='required_field' name ='address' size='12' value='$address' />
        پلاک وسیله نقلیه:
        <input type='text' class='required_field' name ='carid' size='12' value='$carid' /><br/>
        تلفن یا موبایل:
        <input type='text' class='required_field' name ='number' size='12' value='$number' />
        آی دی تلگرام:
        <input type='text' class='required_field' name ='telegram'  value='$telegram' />
        آی دی اینستاگرام:
        <input type='text' class='required_field' name ='instagram'  value='$instagram' />
        آیدی های اینترنتی دیگر:
        <input type='text' class='required_field' name ='socialid'  value='$socialid' /><br>
        توضیحات:
        <textarea  class='formfield' value='$extra' name ='extra' ></textarea> <br/>
<img id='captcha' src='../securimage/securimage_show.php' alt='CAPTCHA Image' /><br/>
<a href='#' onclick=\"document.getElementById('captcha').src = '../securimage/securimage_show.php?' + Math.random(); return false\">[ تعویض کد ]</a>
<br>
<input type='text' name='captcha_code' size='10' placeholder='کد عکس' required />
<p class='fa_text' style='display: none;' id='err_box'></p>
<br/><button type='submit'>ذخیره</button>";


echo "$html_header
$form
$html_footer
<script src='../JalaliJSCalendar-1.4/jalali.js'></script>
<script src='../JalaliJSCalendar-1.4/calendar.js'></script>
<script src='../JalaliJSCalendar-1.4/calendar-setup.js'></script>
<script src='../JalaliJSCalendar-1.4/lang/calendar-fa.js'></script>
<script src='../js/edit.js'></script>";
?>
