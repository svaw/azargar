<?php 
session_start();
include("./defined.php");
require_once '../securimage/securimage.php';
$securimage = new Securimage();
if ($securimage->check($_POST['captcha_code']) == false){exit("<span syle='color:red;'> کد عکس اشتباه است </span>");}
$username=test_input_raw($_POST['username']); # username must be 4 to 10 char
$pass=test_input_raw($_POST['pass']);
$username_length=mb_strlen($username,'utf-8');
$pass_length=mb_strlen($pass,'utf-8');
if($username_length < 4 || $username_length > 10){
exit("<span style='color:red;' dir=rtl >طول نام کاربری باید بین ۵ تا ۱۰ خرف  باشد</span>");}
if($pass_length < 6 || $pass_length > 30){
exit("<span style='color:red;' dir=rtl >طول کلمه عبور باید بین ۶ تا ۳۰ حرف  باشد</span>");}

try{
$db= new PDO("mysql:host=$db_host;dbname=$db_db" , $db_user, $db_pass );
$patern_exist_username="SELECT username from users WHERE username like :username";
$patern_signup="insert into users (username , hashpass) values (:username,:pass)";
$stmt= $db->prepare($patern_exist_username);
$stmt->execute(["username"=>$username]);
// echo sizeof($stmt->fetchAll(PDO::FETCH_COLUMN,0));
if (sizeof($stmt->fetchAll(PDO::FETCH_COLUMN,0)) != 0){exit("<span style='color:red;' dir=rtl >این نام قبلا ثبت شده است.</span>");}
$stmt= $db->prepare($patern_signup);
$hashpass=password_hash($pass,PASSWORD_BCRYPT);
$stmt->execute([':username'=> $username , 'pass' => $hashpass ]);
echo "<span style='color:green;' dir=rtl >ثبت شد </span>";
}
catch(PDOException $e)
    {
    echo  $e->getMessage();
    }
?>
