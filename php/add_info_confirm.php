<?php 
session_start();
$html_title="-افزودن اطلاعات جدید";
require ("./defined.php");
require ("./message_fa.php");
echo $html_header;
 include_once '../securimage/securimage.php';
$securimage = new Securimage();
if ($securimage->check($_POST['captcha_code']) == false) {
  exit($captcha_code_err);
}
if ( ! $ref_id=test_id( $_POST['ref_id'] )){exit("id error");}

$name=$family=$birthdate=$job=$jobtitle=$city=$lang=$address=$carid=$number=$telegram=$instagram=$socialid=$extra="";
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $name=test_input($_POST["name"]);
    $family=test_input($_POST["family"]);
    $birthdate=test_input($_POST["birthdate"]);
    $job=test_input($_POST["job"]);
    $jobtitle=test_input($_POST["jobtitle"]);
    $city=test_input($_POST["city"]);
    $lang=test_input($_POST["lang"]);
    $address=test_input($_POST["address"]);
    $carid=test_input($_POST["carid"]);
    $number=test_input($_POST["number"]);
    $telegram=test_input($_POST["telegram"]);
    $instagram=test_input($_POST["instagram"]);
    $socialid=test_input($_POST["socialid"]);
    $extra=test_input($_POST["extra"]);
}
$deleteid=rand() . rand(); # random_int() does not support in php5.
$hash=password_hash($deleteid,PASSWORD_BCRYPT);
$img_file="../img/no-img.png" ;
if ($_FILES['pic']['tmp_name']){
   $f_mime=explode('/',mime_content_type($_FILES["pic"]["tmp_name"]))[0];
    if ($f_mime == "image" && ($_FILES['pic']['size'] < $image_max_size )) {
         $img_file=md5_file($_FILES["pic"]["tmp_name"]) . "." . pathinfo($_FILES['pic']['name'],PATHINFO_EXTENSION);
        rename($_FILES['pic']['tmp_name'] , "../$faces_dir/$img_file");
    } else {
exit($image_value_err);
    }
}
if ($name.$family.$img_file.$birthdate.$job.$jobtitle.$city.$lang.$address.$carid.$number.$telegram.$instagram.$socialid.$extra == "" ) {
exit( "<h1 class=fa_text> شما هیچ اطلاعاتی را اضافه نکرده اید. </h1>");}
$db = new PDO("mysql:host=$db_host;dbname=$db_db", $db_user, $db_pass);
// $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$cmd="INSERT INTO list(img,name,family,birthdate,job,jobtitle,city,lang,address,carid,number,telegram,instagram,socialid,extra,hash,ref_id,h_counter)
VALUES (:img,:name,:family,:birthdate,:job,:jobtitle,:city,:lang,:address,:carid,:number,:telegram,:instagram,:socialid,:extra,:hash,:ref_id,0)";
$stmt = $db->prepare($cmd);
$data=['img' => $img_file,
    'name' => $name ,
    'family' => $family ,
    'birthdate' => $birthdate ,
    'job' => $job ,
    'jobtitle' => $jobtitle ,
    'city' => $city ,
    'lang' => $lang ,
    'address' => $address ,
    'carid' => $carid ,
    'number' => $number ,
    'telegram' => $telegram ,
    'instagram' => $instagram ,
    'socialid' => $socialid ,
    'extra' => $extra ,
    'hash' => $hash,
    'ref_id' => $ref_id];
try {
$stmt->execute($data);
$my_url="$website_base_url/php/view.php?id=$ref_id";
echo "<div class='fa_text'><center><h1> شماره ثبت شما: </h1>"
. "<br><h2>$deleteid</h2></br>توجه: شما فقط با این کد می توانید اطلاعاتی را که ثبت کرده اید را ویرایش ویا حذف کنید."
        . "<br>"
        . "لینک دايمی به آزارگر شما:"
        . "<br><a href=$my_url>$my_url</a>"
        . "<br>اشتراک گذاری صفخه آزارگر در شبکه های اجتماعی :<br>"
        . "<a href='https://www.facebook.com/sharer.php?u=$my_url'><img class='sharelink' src=../img/facebook.png /></img></a>
        <a href='https://twitter.com/intent/tweet?url=$my_url'><img class='sharelink' src=../img/twitter.png /><img /></a>
        <a href='https://telegram.me/share/url?url=$my_url'><img class='sharelink' src=../img/tel.png /><img /></a>"
        . "</center></div>";
}
catch(Exception $e) {
//     echo 'Exception -> ';
//    var_dump($e->getMessage());
    echo "<div class='fa_text'><h1> متاسفانه در بانک اطلاعاتی خطایی پیش آمد با ایمیل زیر تماس بگیرید </h1></div>";
}

 echo $html_footer;
 ?>
