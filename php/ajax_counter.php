<?php 
include ("./defined.php");

if ( ! $id=test_id( $_GET['id'] )){exit;}
$db = new PDO("mysql:host=$db_host;dbname=$db_db", $db_user, $db_pass);
$stmt = $db->prepare("UPDATE list SET h_counter = h_counter + 1 WHERE id = :id");
$stmt->execute(["id" => $id ]);
$stmt = $db->prepare("SELECT h_counter FROM list WHERE id = :id");
$stmt->execute(["id" => $id ]);
echo num_to_fa($stmt->fetchAll(PDO::FETCH_COLUMN, 0)[0]);
?>
