<?php 
$html_title="-گزارش یک آزار ";
$html_css_file="
<style>
input[type='checkbox']{
  
  outline: 2px solid orange;
  box-shadow: none;
  font-size: 2em;
}
::placeholder{
color:blue;
font-size=large;
}
/*input[type='checkbox']:nth-of-type(odd){outline: 2px solid purple;}*/
</style>";
require ( "./php/defined.php");
echo $html_header;
$url_patern="$website_base_url".'/php/view.php\?id=\d(\d*)$';
$ref_id_html_field="لینک به صفحه آزارگر: 
<br /><input type=url name=ref_link pattern=$url_patern placeholder='لینک صفحه آزارگر' />";
if (isset($_GET['ref_id'])) {
if ( ! $ref_id=test_id( $_GET['ref_id'] ) ) { exit;}
$ref_id_html_field="<input type=hidden value=$ref_id readonly />";
}
?>
<div class=fa_text style="margin-left:auto;margin-right:auto;width:70%">
<h1>ثبت آزار </h1>
<form id=add_harassment method=post action="php/add_harassment_confirm.php" onsubmit="checkform();return false;" enctype="multipart/form-data" >
<? echo $ref_id_html_field ?>
<br />
لطفا یک دسته را انتخاب کنید: <br>
<select name="category" >
   <option style='direction:rtl;float:right;' value="0">انتخاب نشده</option>
   <option style='direction:rtl;float:right;' value="1">آزارخیابانی</option>
   <option style='direction:rtl;float:right;' value="2"> آزار در محیط کار </option>
   <option style='direction:rtl;float:right;' value="3">آزار در  دانشگاه یا  سایر مکان های آموزشی</option>
   <option style='direction:rtl;float:right;' value="4">آزار در  ادارات دولتی</option>
   <option style='direction:rtl;float:right;' value="5">آزار در مکان های تفریحی و گردشی</option>
   <option style='direction:rtl;float:right;' value="6">آزار در مکان های بخش خصوصی</option>
   <option style='direction:rtl;float:right;' value="6">آزار در بیمارستان و مراکز درمانی</option>
   <option style='direction:rtl;float:right;' value="6">آزار تلفنی</option>
   <option style='direction:rtl;float:right;' value="6">آزار در محیط های مبتنی بر اینترنت</option>
   <option style='direction:rtl;float:right;' value="6">آزار در محیط های خانوادگی و آشنایان نزدیک</option>
   <option style='direction:rtl;float:right;' value="6">آزار در محیط های  ورزشی</option>
   <option style='direction:rtl;float:right;' value="6">آزار در هنگام استفاده از خدمات  حمل و نقل عمومی</option>
   <option style='direction:rtl;float:right;' value="6">آزار در  مکانهای فرهنگی و سینما</option>
  </select>
  <hr>
شرح اتفاق : <br /> 
<textarea name="description"  placeholder="مثلا چه اتفاقی برایتان افتاد؟ چه حسی داشتید؟ دیگران چه واکنشی داشتند؟ و..."  rows=10 cols=60 required /></textarea><br>
در کجا مورد آزار قرار گرفتید؟ 
<input type=radio name=country value='IR' onclick="get_country('hide');" checked/> <label for=country>داخل کشور</label>
<input type=radio name=country  onclick="get_country('show');" /> <label for=country>خارج از کشور</label> <br>
<select name=country id=f_country onchange="othercountry('country',this.value);" style="display:none;"></select>
<select name=province id=f_province onchange="othercountry('state',this.value);" style="display:none;"></select>
<select name=city id=f_city style="display:none;"></select>
<select name="province" id="province" onchange="updatecity()" dir="rtl"><option value="0">یک استان را انتخاب کنید</option><option value="19">آذربایجان شرقی</option><option value="20">آذربایجان غربی</option><option value="22">اردبیل</option><option value="2">اصفهان</option><option value="21">ایلام</option><option value="23">بوشهر</option><option value="1">تهران</option><option value="13">چهارمحال بختیاری</option><option value="25">خراسان رضوی</option><option value="26">خراسان جنوبی</option><option value="27">خراسان شمالی</option><option value="24">خوزستان</option><option value="28">زنجان</option><option value="29">سمنان</option><option value="30">سیستان و بلوچستان</option><option value="3">فارس</option><option value="5">قزوین</option><option value="4">قم</option><option value="9">كردستان</option><option value="7">كرمان</option><option value="8">كرمانشاه</option><option value="6">كهكلویه و بویر احمد</option><option value="17">گلستان</option><option value="18">گیلان</option><option value="10">لرستان</option><option value="11">مازندران</option><option value="12">مركزی</option><option value="15">هرمزگان</option><option value="14">همدان</option><option value="16">یزد</option></select>
  <select name="city" id="city" dir="rtl"><option value="0">ابتدا استان را انتخاب کنید</option></select>
  <br>
  درهنگام آزار چندسال داشتید؟ 
<select name="age"><option value=0>انتخاب نشده </option>
<? for ($i = 1 ; $i < 60 ; $i++ ) { echo "<option value=$i >".num_to_fa($i)."</option>"; } echo "<option value=60>+۶۰</option></select>"; ?>
<hr>
<h3>فایلهای مربوط به این آزار </h3>
عکس : </br> 
<input name="image" id="image" type="file" accept="image/*" onchange="show_address('image','img_address')" style='display:none' /> 
<label for="image" ><img src=img/i_mime.png /></label><span style='float:left' dir=ltr id=img_address></span><br>
فایل صوتی : <br>
<input name="audio" id="audio" type="file" accept="audio/*" onchange="show_address('audio','audio_address')" style='display:none' /> 
<label for="audio" ><img src=img/a_mime.png /></label><span style='float:left' dir=ltr id=audio_address></span><br>
ویدئو: <br> 
<input name="video" id="video" type="file" accept="video/*" onchange="show_address('video','video_address')" style='display:none' /> 
<label for="video" ><img src=img/v_mime.png /></label><span style='float:left' dir=ltr id=video_address></span><br>
فایل zip (برای ارسال فایلهایی که عکس و صوت و ویدئو نیستند) : <br> 
<input name="zip" id="zip" type="file" accept=".zip" onchange="show_address('zip','zip_address')" style='display:none' /> 
<label for="zip" ><img src=img/z_mime.png /></label><span style='float:left' dir=ltr id=zip_address></span><br>
<hr>
<h3> پرسشنامه </h3>
<div style='line-height:2em'>

آیا به مسئول فرد خاطی مثل رئیس  مجموعه یا کارفرما یا والدین فرد اطلاع دادید؟
 <input id=q1y name="report_to_boss" value="1" type="checkbox" onclick="uncheck('q1n');" />بله <!--q1y means question 1 YES button-->
 <input id=q1n name="report_to_boss" value="0" type="checkbox" onclick="uncheck('q1y');" />خیر
<br>

آیا از حمایت دوست پسر/پارتنر/نامزد/شوهر راضی بودید؟ 
<input id=q2n name=partner_support value="0" type="checkbox" onclick="uncheck('q2a,q2y');" /> خیر
<input id=q2y name=partner_support value="1" type="checkbox" onclick="uncheck('q2a,q2n');" /> بله
<input id=q2a name=partner_support value="2" type="checkbox" onclick="uncheck('q2y,q2n');" /> اطلاع نداشتند!

<br>
آیااز حمایت خانواده راضی بودید؟
<input id=q3n name=familly_support value="0" type="checkbox" onclick="uncheck('q3a,q3y');" /> خیر
<input id=q3y name=familly_support value="1" type="checkbox" onclick="uncheck('q3a,q3n');" /> بله
<input id=q3a name=familly_support value="2" type="checkbox" onclick="uncheck('q3y,q3n');" /> اطلاع نداشتند!
<br>
آیا از حمایت دوستان راضی بودید؟
<input id=q4n name=friend_support value="0" type="checkbox" onclick="uncheck('q4a,q4y');" /> خیر
<input id=q4y name=friend_support value="1" type="checkbox" onclick="uncheck('q4a,q4n');" /> بله
<input id=q4a name=friend_support value="2" type="checkbox" onclick="uncheck('q4y,q4n');" /> اطلاع نداشتند!
<br>
 آیا به پلیس گزارش دادید؟
<input id=q5n name=report_to_police value="0" type="checkbox" onclick="uncheck('q5y');" /> خیر
<input id=q5y name=report_to_police value="1" type="checkbox" onclick="uncheck('q5n');" /> بله

<br>
آیا پلیس پرونده تشکیل داد؟ 
<input id=q6n name=rap_sheet value="0" type="checkbox" onclick="uncheck('q6y');" /> خیر
<input id=q6y name=rap_sheet value="1" type="checkbox" onclick="uncheck('q6n');" /> بله

<br>
آگر شکایت رسمی تنظیم کردید آیا فرد خاطی مجازات شد؟
<input id=q7n name=punished value="0" type="checkbox" onclick="uncheck('q7y');" /> خیر
<input id=q7y name=punished value="1" type="checkbox" onclick="uncheck('q7n');" /> بله

<br>
آیا فرد خاطی بعد از مجازات به آزار خود ادامه داد/می دهد؟
<input id=q8n name=keep_on value="0" type="checkbox" onclick="uncheck('q8y');" /> خیر
<input id=q8y name=keep_on value="1" type="checkbox" onclick="uncheck('q8n');" /> بله

<br>
به طور کلی از برخورد و اقدامات پلیس و دستگاه قضایی درخصوص پرونده خودتان چقدر راضی هستید؟ 
<select name="police_raiting"><option value=0></option><option value=1 >اصلا </option><option value=2>کم </option><option value=3>تاحدودی</option><option value=4>زیاد</option><option value=5> کامل </option></select>
<br>
آیا پس از آزار به مشاور یا روان شناس و یا سایر منابع مربوطه برای درمان آسیبهای ناشی از آزار مراجعه کرده اید؟

<input id=q9n name=therapy value="0" type="checkbox" onclick="uncheck('q9y');" /> خیر
<input id=q9y name=therapy value="1" type="checkbox" onclick="uncheck('q9n');" /> بله
<br>
به نظر شما چرا فرد خاطی دست به آزاردادن بقیه می زند؟
<br>
<textarea cols=60 rows=10 name=why placeholder="به نظر شما چه عاملی باعث  تبدیل یک فرد به یک  آزارگر می شود؟" /></textarea>
</div>
<img id="captcha" src="./securimage/securimage_show.php" alt="CAPTCHA Image" /><br/>
<a href="#" onclick="document.getElementById('captcha').src = 'securimage/securimage_show.php?' + Math.random(); return false">[ تعویض کد ]</a>
<br>
<input type="text" name="captcha_code" size="10" placeholder="کد عکس" required />
<p class="fa_text" style="display: none;" id="err_box"></p>
<br/><button type="submit">ثبت</button>
</form>
</div>
<script src='./js/add_harassment.js'> </script>
 <script src='./js/IR_city.js'></script>
<? echo $html_footer; ?>
