function show_address (input_id , target_id) {
    var address_box = document.getElementById(target_id);
    var value = document.getElementById(input_id).value.split('\\');
    var filename = value[value.length-1];
    address_box.innerHTML = filename;
 };
 function uncheck(ids){
 var id_arr=ids.split(',');
 for ( var i =0 ; i< id_arr.length ; i++){
 document.getElementById(id_arr[i]).checked=false;
 }
 }
 
 function get_country(state){
 if (state == "show"){
    document.getElementById('province').style.display="none";
    document.getElementById('province').disabled="disabled";
    document.getElementById('city').style.display="none";
    document.getElementById('city').disabled="disabled";

    document.getElementById('f_province').disabled=false;
    document.getElementById('f_city').disabled=false;
//     document.getElementById('f_province').style.display="inline";
//     document.getElementById('f_city').style.display="inline";
    
    var country_box=document.getElementById('f_country');
    var country_obj = new XMLHttpRequest();
    country_obj.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      country_box.innerHTML = this.responseText;
      country_box.style.display="inline";
      country_box.disabled=false;
        }
    }
    country_obj.open("GET" , "../misc/country_code" );
    country_obj.send();
    }
if (state == "hide"){
    document.getElementById('province').style.display="inline";
    document.getElementById('city').style.display="inline";
    document.getElementById('province').disabled=false;
    document.getElementById('city').disabled=false;

    document.getElementById('f_country').disabled="disabled";
    document.getElementById('f_province').disabled="disabled";
    document.getElementById('f_city').disabled="disabled";
    
    document.getElementById('f_country').style.display="none";
    document.getElementById('f_province').style.display="none";
    document.getElementById('f_city').style.display="none";
    
 }
 }
 function othercountry(region , value ){
var othercountry_obj = new XMLHttpRequest();
var output="";
switch ( region ){
case "country":
    var f_province_box=document.getElementById('f_province');
    var f_city_box = document.getElementById("f_city");
    if ( value == 0 ){
    f_province_box.innerHTML="<option value=0>استان/ایالت</option>";
    f_city_box.innerHTML="<option value=0>شهر</option>";
    return ;
    }
    output = region+":"+value;
    othercountry_obj.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
    f_province_box.innerHTML=this.responseText;
    f_province_box.style.display="inline";
    f_province_box.disabled=false;
    f_city_box.innerHTML="<option value=0>شهر</option>";
    f_city_box.disabled=false;
    
    } }
    
break;
case "state":
var country = document.getElementById('f_country').value;
output="country"+":"+ country + "-" + region + ":" + value;
var f_city_box=document.getElementById('f_city');
othercountry_obj.onreadystatechange= function (){
if ( this.readyState == 4 && this.status ==200) {
    f_city_box.innerHTML=this.responseText;
    f_city_box.style.display="inline";
    }}
break;
}
othercountry_obj.open("POST", "./php/ajax_othercountry.php");
othercountry_obj.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
othercountry_obj.send(encodeURI("othercountry_req=" + output));

    }
