 <?php 
session_start();
$html_title="-ویرایش";
require ("./config.php");
require ("defined.php");
include ("./message_fa.php");
echo $html_header;
 include_once '../securimage/securimage.php';
$securimage = new Securimage();
if ($securimage->check($_POST['captcha_code']) == false) {
  exit("$captcha_code_err");
}
  
$name=$family=$birthdate=$job=$jobtitle=$city=$lang=$address=$carid=$number=$telegram=$instagram=$socialid=$extra="";
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $id=test_input($_POST["id"]);
    $deleteid=test_input($_POST["key"]);
    $name=test_input($_POST["name"]);
    $family=test_input($_POST["family"]);
    $birthdate=test_input($_POST["birthdate"]);
    $job=test_input($_POST["job"]);
    $jobtitle=test_input($_POST["jobtitle"]);
    $city=test_input($_POST["city"]);
    $lang=test_input($_POST["lang"]);
    $address=test_input($_POST["address"]);
    $carid=test_input($_POST["carid"]);
    $number=test_input($_POST["number"]);
    $telegram=test_input($_POST["telegram"]);
    $instagram=test_input($_POST["instagram"]);
    $socialid=test_input($_POST["socialid"]);
    $extra=test_input($_POST["extra"]);
}

if ( ! $id=test_id( $_POST['id'] ) or ! $deleteid=test_id( $_POST['key'] ) ){
exit("<h1> اطلاعات به درستی دریافت نشده است. دوباره تلاش کنید یا به ما اطلاع دهید </h1> $html_footer");
 }

$img_file="../img/no-img.png" ;
if ($_FILES['pic']['tmp_name']){
    $f_mime=explode('/',mime_content_type($_FILES["pic"]["tmp_name"]))[0];
    if ($f_mime == "image" && ($_FILES['pic']['size'] < $image_max_size )) {
         $img_file=md5_file($_FILES["pic"]["tmp_name"]). "." . pathinfo($_FILES['pic']['name'],PATHINFO_EXTENSION);
        rename($_FILES['pic']['tmp_name'] , "../$faces_dir/$img_file");
    } else {
echo "<div class=fa_text><h1>خطا! حجم یا فرمت عکس غیرمجاز است لطفا به 
<a href='javascript:history.go(-1)'>صفحه قبل </a>برگردید و  دوباره امتحان کنید</h1>";
 echo $html_footer;
  exit; 
    }
}
$check_required_field_less=0;
$hi_check_result = true; //powerful! (form will be valid only by one item)
$less_check_result = true;
$pic="valid";
if ($img_file == "../img/no-img.png") {
    $pic=""; //only used for checking required form field
}
foreach ([$name,$family,$job,$city] as $less_item)
{
    if (!empty($less_item)) {
        $check_required_field_less++;
    }
}
if ( $check_required_field_less < 3 ){
    $less_check_result=false;
}
if ($pic.$address.$carid.$number.$telegram.$instagram.$socialid == ""){
    $hi_check_result = false;
}
if (!($hi_check_result || $less_check_result)){
    exit("<div class='fa_text'><h1>اطلاعاتی که وارد کردید آن قدر عمومی است که ما تمی توانیم آنها را  ثبت کنیم لطفا جزییات بیشتری را وارد کنید.</h1></div> $html_footer");
}
$db = new PDO("mysql:host=$db_host;dbname=$db_db", $db_user, $db_pass);
// $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$get_hash_patern="SELECT hash FROM list WHERE id = :id ";
$stmt = $db->prepare($get_hash_patern);
$stmt->execute(['id' => $id]);
$hash=$stmt->fetchAll(PDO::FETCH_COLUMN, 0)[0];
if (! password_verify($deleteid, $hash)) {
exit ("<h1> کد ثبت شما اشتباه است لطفا مطمئن شوید که کد را درست وارد کردید و توجه داشته باشید که شما فقط می توانید اطلاعاتی را که خودتان وارد کردید را ویرایش کنید. می توانید با ما تماس بگیرید. </h1> $html_footer");
}

$update_info_patern="UPDATE list SET img = :img ,
name = :name ,
family = :family ,
birthdate = :birthdate ,
job = :job ,
jobtitle = :jobtitle ,
city = :city ,
lang = :lang ,
address = :address ,
carid = :carid ,
number = :number ,
telegram = :telegram ,
instagram = :instagram ,
socialid = :socialid ,
extra = :extra  WHERE id = :id
";
$stmt = $db->prepare($update_info_patern);
$data=['img' => $img_file,
    'name' => $name ,
    'family' => $family ,
    'birthdate' => $birthdate ,
    'job' => $job ,
    'jobtitle' => $jobtitle ,
    'city' => $city ,
    'lang' => $lang ,
    'address' => $address ,
    'carid' => $carid ,
    'number' => $number ,
    'telegram' => $telegram ,
    'instagram' => $instagram ,
    'socialid' => $socialid ,
    'extra' => $extra ,
    'id' => $id ];
try {
$stmt->execute($data);
echo "<h1 style='color:green'> اطلاعات با موفقیت به روز شد. </h1>  " ;
}
catch(Exception $e) {
//     echo 'Exception -> ';
//    var_dump($e->getMessage());
    echo "<div class='fa_text'><h1> متاسفانه در بانک اطلاعاتی خطایی پیش آمد با ایمیل زیر تماس بگیرید </h1></div>";
}
echo $html_footer;
 ?>
