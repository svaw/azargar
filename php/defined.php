<?PHP
if (! isset($html_title)){$html_title="";}
if (! isset($html_css_file)){$html_css_file="";}

$fix_relative_root_path=".."; # with / its equal to ../ and used by script in directory php.
if(! include ("../php/config.php")){ # we cant use ./config because its limit only on php Directory and its not work on admin directory(setup.php).
$fix_relative_root_path="."; # with / its equal to ./ and used by script in directory root.
include ("./php/config.php");
}
    // html  part define
$html_header =  "<!DOCTYPE html>
<html lang='fa'>
  <head>
    <title> $website_title $html_title </title>
    <meta   name='viewport' content='width=device-width, initial-scale=1.0'>
    <meta  charset='utf-8'>
    <link rel='stylesheet' type='text/css' href='$fix_relative_root_path/css/style.css'>
    $html_css_file
    </head>
  <body>
      <div id='header' >
      <center>
          <a href='$fix_relative_root_path/index.php'> صفحه اصلی </a>
          <a href='$fix_relative_root_path/gallery.htm'> گالری </a>
          <a href='$fix_relative_root_path/add_p.php'> ثبت آزارگر  </a>
          <a href='$fix_relative_root_path/add_harassment.php'> ثبت آزار  </a>
          <a href='$fix_relative_root_path/search_p.php' > جست و جو </a>
          <a href='$fix_relative_root_path/src.php'> توسعه دهندگان  </a>
          </center>
      </div>";
      $html_footer = '<div id="footer" > created with &#10084;</div>  </body></html>';
   
   // data base define section 

$patern_output_by_id="SELECT img,name,family,birthdate,job,jobtitle,city,lang,address,carid,number,telegram,instagram,socialid,extra,h_counter FROM list WHERE id = :id ";
$pattern_h_output_by_id="SELECT ref_id , reported , description , image_file , audio_file , video_file , zip_file ,why FROM harassment WHERE id = :id";
    // function define section

function num_to_eng($fa_string){
    return str_replace(
    array('۰','۱','۲','۳','۴','۵','۶','۷','۸','۹'),
    array('0','1','2','3','4','5','6','7','8','9'),
            $fa_string);
  }

function num_to_fa($en_string){
            return str_replace(
            array('0','1','2','3','4','5','6','7','8','9'),
            array('۰','۱','۲','۳','۴','۵','۶','۷','۸','۹'),
            $en_string);
            }

            function test_input($user_input) {
  $user_input = trim($user_input);
  $user_input = stripslashes($user_input);
  $user_input = htmlspecialchars($user_input);
  $user_input = preg_replace('/\s+/', ' ',$user_input);
  $user_input = num_to_eng($user_input);
  return $user_input;
}
            function test_input_raw($user_input) {
  $user_input = trim($user_input);
  $user_input = stripslashes($user_input);
  $user_input = htmlspecialchars($user_input);
  $user_input = preg_replace('/\s+/', ' ',$user_input);
  return $user_input;
}
function test_id ($id){
$id=test_input($id);
if (is_numeric($id) && intval($id) >= 1 ) {
return $id; 
}else {
return false;}
}

function test_num ($num , $min , $max ){
$num=test_input($num);
if (is_numeric($num) && intval($num) >= $min && intval($num) <= $max ) {
return intval($num); 
}else {
return false;}
}

function test_checkbox($input_name , $num_of_option){ #num_of_option for ex: if two checkbox exist num_of_option = 2
if(isset($_POST["$input_name"])) {
$code_range_a=0;
$code_range_b=$num_of_option-1;
$result=test_num($_POST["$input_name"] , $code_range_a , $code_range_b);
if ( $result === false) { $result = "";} # if an error occurred we ignere it and continue because its make user bored
return $result;
}else { return "";}
}

#zip info 
function zip_info($zip_file){
$za = new ZipArchive();
$za->open($zip_file);
global $malicious_file_war;
$zip="<table>";
$zip .= "<caption>$malicious_file_war</caption>";
$zip .= "<tr><td>اندازه کل فایل</td><td>".num_to_fa (filesize($zip_file))."</td></tr>";
$zip .="<tr><td> لینک فایل</td><td><a href=$zip_file>دانلود</a></td></tr>";
$zip .= "<tr><td> تعداد فایل</td><td>". num_to_fa($za->numFiles) . "</td></tr>";
$zip .= "<tr><th>نام فایل </th><th>اندازه (بایت) </th></tr>";

for ($i=0; $i<$za->numFiles;$i++) {
    $zip_index=$za->statIndex($i);
    $zip .= "<tr><td>" . $zip_index['name'] . "</td><td>".num_to_fa($zip_index['size']) ."</td></tr>";
}
$zip .= "</table>";
return $zip;
// echo $zip;
}


?>
