 
 <?php
$html_title="- ویرایش آزار";
$html_css_file="<link rel='stylesheet' type='text/css' href='../css/h_edit.css'>"; #all html_* must be before include command!
require ("./config.php");
require ("./defined.php");
include ("./message_fa.php");
echo $html_header;

if ( ! $id=test_id( $_GET['id'] )){exit;}

$db= new PDO ("mysql:host=$db_host;dbname=$db_db" , $db_user , $db_pass );
$stmt = $db->prepare($pattern_h_output_by_id);
if ( ! $stmt->execute (['id' => $id])) { exit("$db_err");}

$stmt->bindcolumn('ref_id' , $ref_id);
$stmt->bindcolumn('description' , $description);
$stmt->bindcolumn('image_file' , $image_file);
$stmt->bindcolumn('audio_file' , $audio_file);
$stmt->bindcolumn('video_file' , $video_file);
$stmt->bindcolumn('zip_file' , $zip_file);
$stmt->bindcolumn('why' , $why);
$stmt->fetch(PDO::FETCH_BOUND);

$url_patern="$website_base_url".'/php/view.php\?id=\d(\d*)$';
$size_of_ref_field=strlen("$website_base_url/php/view.php?id=")+7; # maximum character for id is 7(its not important) .
$ref_id_html_field="لینک به صفحه آزارگر: 
<br /><input type=url  dir=ltr style='text-align:center;' size=$size_of_ref_field name=ref_link pattern=$url_patern placeholder='لینک صفحه آزارگر'";
if (!empty($ref_id) && test_id($ref_id) ) {
$ref_link="$website_base_url/php/view.php?id=$ref_id";
$ref_id_html_field .="value=$ref_link";
}
$ref_id_html_field .= " />";

$image = $audio = $video = $zip ="موردی منصوب به این آزار وجود ندارد";

if ($image_file != "undifined") { $image="<input id=delete_image type=checkbox name=delete_image /> <label for=delete_image />حذف </label><img style='max-width:100%' src=$image_file />";}

if ($audio_file != "undifined") { $audio = "<input id=delete_audio type=checkbox name=delete_audio /> <label for=delete_audio />حذف </label>". html5_multimedia("audio" , $audio_file); }

if ($video_file != "undifined") { $video = "<input id=delete_video type=checkbox name=delete_video /> <label for=delete_video />حذف </label>". html5_multimedia("video" , $video_file);}

if ($zip_file != "undifined"){ $zip ="<input id=delete_zip type=checkbox name=delete_zip /> <label for=delete_zip />حذف </label>". zip_info($zip_file); }

?>
<form action='h_edit_confirm.php' onsubmit='return checkForm()' method='post' enctype='multipart/form-data'>
        <input type='hidden' name=id value=<? echo $id; ?> /> <br>
<div class=fa_text style="margin-left:auto;margin-right:auto;width:70%">
<h1>ویرایش </h1>

        شماره ثبت: <span style='color:red'>*</span>
        <input type='text'  id='key' name='key'  placeholder='شماره ثبت' required /><br>
<? echo $ref_id_html_field; ?><br>
        
        شرح اتفاق : <br /> 
<textarea name="description"   rows=10 cols=60 required /><? echo $description; ?></textarea><br>
<hr>
<h3>فایلهای مربوط به این آزار </h3>
عکس : </br> 
<input name="image" id="image" type="file" accept="image/*" onchange="show_address('image','img_address')" style='display:none' /> 
<label for="image" ><img src=../img/i_mime.png /></label><span style='float:left' id=img_address></span><br>
عکس کنونی :

<?php echo $image; ?><br>
فایل صوتی : <br>
<input name="audio" id="audio" type="file" accept="audio/*" onchange="show_address('audio','audio_address')" style='display:none' /> 
<label for="audio" ><img src=../img/a_mime.png /></label><span style='float:left' id=audio_address></span><br>
فایل صوتی کنونی: 

<?php echo $audio; ?><br>
ویدئو: <br> 
<input name="video" id="video" type="file" accept="video/*" onchange="show_address('video','video_address')" style='display:none' /> 
<label for="video" ><img src=../img/v_mime.png /></label><span style='float:left' id=video_address></span><br>
ویدئو کنونی : 

<?php echo $video; ?><br>
فایل zip (برای ارسال فایلهایی که عکس و صوت و ویدئو نیستند) : <br> 
<input name="zip" id="zip" type="file" accept=".zip" onchange="show_address('zip','zip_address')" style='display:none' /> 
<label for="zip" ><img src=../img/z_mime.png /></label><span style='float:left' id=zip_address></span><br>
فایل زیپ کنونی: 

<?php echo $zip; ?><br>
<hr>
به نظر شما چرا فرد خاطی دیگران را آزار می دهد؟
<br>
<textarea cols=60 rows=10 name=why /><? echo $why; ?></textarea><br>
<img id='captcha' src='../securimage/securimage_show.php' alt='CAPTCHA Image' /><br/>
<a href='#' onclick="document.getElementById('captcha').src = '../securimage/securimage_show.php?' + Math.random(); return false;">[ تعویض کد ]</a>
<br>
<input type='text' name='captcha_code' size='10' placeholder='کد عکس' required />
<br/><button type='submit'>ذخیره</button>
</div>
</form>
<script src='../js/add_harassment.js'> </script>
<? echo "$html_footer";?>
