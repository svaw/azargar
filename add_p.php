<?php 
$html_title="معرفی یک آزارگر";
$html_css_file="<link rel=stylesheet type=text/css href=./css/add.css>
    <link rel=stylesheet type=text/css media=all href=JalaliJSCalendar-1.4/skins/aqua/theme.css title=Aqua />
    <link rel='alternate stylesheet' type=text/css media=all href=JalaliJSCalendar-1.4/skins/calendar-blue.css title=winter />";
require("./php/defined.php");
echo $html_header;
?>
  <center><h1> صفحه ثبت اطلاعات آزارگر</h1>
  </center>
  <div id='content'>
      <div dir="rtl" id='left'> 
          <ul>
              <li>این صفحه مخصوص ثبت اطلاعات آزارگر می باشد. اطلاعات خودتان را وارد نکنید!</li>
              <li> هیچ کدام از فیلدها اجباری نیستند. ولی حداقل یکی از فیلدهایی که حاشیه نارنجی دارند باید پر شده باشند. و یا اینکه از فیلدهایی که حاشیه بنفش دارند حداقل سه مورد را پرکنید.</li>
              <li>حجم عکس باید کمتر از یک مگابایت باشد. عکس های حجیم تر را می توانید در سایت دیگری آپلود کنید و لینک ان را در قسمت توضیحات وارد کنید.</li>
              <li>از قسمت آی دی های مجازی می توانید برای ثبت آزارهای اینترنتی استفاده کنید.</li>
              <li>از قسمت پلاک خودرو می توانید برای ثبت مزاحمت های خیابانی (خودرو یا موتور سیکلت) استفاده کنید.</li>
              <li>از قسمت شماره تلفن می توانید برای ثبت مزاحمت های تلفنی استفاده کنید.</li>
              <li>در قسمت آدرس می توانید آدرس محل زندگی، محل کار یا هر ادرس دیگری که به آزارگر مربوط است را بنویسید .</li>
              <li>گزینه زبان و لهجه فقط برای مشخص تر شدن آزارگر از بقیه افراد همنام ومشابه هست. </li>
              <li>قسمت توضیحات مربوط به اطلاعات اضافی است که فیلدی برای آن تعبیه نشده است.مثل قد،وزن عینکی بودن، سیگاری بودن، تیپ و ظاهر  و...  .همچنین می توانید از آن برای نوشتن شرح آزار استفاده کنید.</li>
              <li>در تکمیل فرم دقیق باشید. حذف یا اصلاح گزارشات محدود است.</li>
              <li>به منظور آزادی عمل بیشتر، در هنگام پرکردن فیلدها نیاز به رعایت الگو و سبک خاصی نیستید.</li>
              <li> در صورت نیاز می توانید در هر فیلد چند  مورد را اضافه کنید مثلا در قسمت آدرس بنویسید:آدرس محل کار:خیابان فلان واحد فلان آدرس محل اقامت:خیابان فلان کوچه بهمان.</li>
              <li>ما تهیه کنندگان سایت ربات نیستیم و موارد مشکوک توسط انسان چک خواهندشد.</li>
              <li>این صفحه هیچ قدرت و اعتبار قانونی ندارد. و فقط جهت گردآوری اطلاعات مردمی است.</li>
              <li>این پروژه داوطلبانه است  و به خاطر پول ساخته نشده و به همین خاطر  از همه نظرات ، پیشنهادات، تغییرات و همکاری ها استقبال می کنیم.</li>
              
              
          </ul>
      </div>
  <div id='right'>
      <form action="php/add.php" onsubmit="return checkForm()" method="post" enctype="multipart/form-data">
        <input type="text" class="required_field_less" name="name" size="12" placeholder="نام" />
        <input type="text" class="required_field_less" name="family" size="12" placeholder="نام خانوادگی" /><br/><br/>
        <input name="pic" id="pic" type="file" accept="image/*" onchange="preview(event)" style="display:none" >
        <label id="ilabel" class="formfield required_field" for="pic" style="font-family: 'B Yekan+';">انتخاب تصویر آزارگر</label>
        <img id="im_preview"/>
        <a id="im_rst" onclick="rm_im()">[حذف عکس ] </a><br/><br/>
        <input name="birthdate" id="date_input" size="12" type="text" placeholder="سال یا تاریخ تولد" >
        <img src="img/cal.jpg" id="date_btn"><br/>
        <input type="text" class="required_field_less" name="job" size="12"  placeholder="شغل" />
        <input type="text" name="jobtitle" size="12" placeholder="رتبه سازمانی" /><br/>
        <input type="text" class="required_field_less" name="city" size="12" placeholder="شهر" />
        <input type="text" name="lang" size="12" placeholder="لهجه/زبان" /><br/>
        <input type="text" class="required_field" name="address" size="12" placeholder="آدرس"/>
        <input type="text" class="required_field" name="carid" size="12" placeholder="شماره پلاک "/><br/>
        <input type="text" class="required_field" name="number" size="12" placeholder="موبایل یا تلفن"/>
        <input type="text" class="required_field" name="telegram"  placeholder="ID تلگرام"/>
        <input type="text" class="required_field" name="instagram"  placeholder="ID اینستاگرام"/>
        <input type="text" class="required_field" name="socialid"  placeholder="سایت‌ها و شبکه‌های اجتماعی دیگر"/><br>
        <textarea  class="formfield" placeholder="توضیحات، شرح اتفاق یا هر اطلاعات دیگری که می خواهید در اینجا ثبت شود" name="extra" ></textarea> <br/>
<img id="captcha" src="./securimage/securimage_show.php" alt="CAPTCHA Image" /><br/>
<a href="#" onclick="document.getElementById('captcha').src = 'securimage/securimage_show.php?' + Math.random(); return false">[ تعویض کد ]</a>
<br>
<input type="text" name="captcha_code" size="10" placeholder="کد عکس" required />
<p class="fa_text" style="display: none;" id="err_box"></p>
<br/><button type="submit">ثبت</button>
</form>
      </div>
</div>
<script src="JalaliJSCalendar-1.4/jalali.js"></script>
<script src="JalaliJSCalendar-1.4/calendar.js"></script>
<script src="JalaliJSCalendar-1.4/calendar-setup.js"></script>
<script src="JalaliJSCalendar-1.4/lang/calendar-fa.js"></script>
<script src="js/add.js"></script>
<?php echo $html_footer; ?>
