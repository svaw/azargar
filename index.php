<?php
$html_title="صفحه اصلی";
 $html_css_file='<link rel="stylesheet" type="text/css" href="css/index.css">';
require("php/defined.php");
echo $html_header;
echo '<div id="recentadded">';
$db = new PDO("mysql:host=$db_host;dbname=$db_db", "$db_user", "$db_pass");
$stmt = $db->query('select img,name,family,birthdate,job,jobtitle,city,lang,address,carid,number,telegram,instagram,socialid,extra from list  order by id desc limit 5');
    $stmt->bindColumn('img' , $img);
    $stmt->bindColumn('name' , $name);
    $stmt->bindColumn('family' , $family);
    $stmt->bindColumn('birthdate' , $birthdate);
    $stmt->bindColumn('job' , $job);
    $stmt->bindColumn('jobtitle' , $jobtitle);
    $stmt->bindColumn('city' , $city);
    $stmt->bindColumn('lang' , $lang);
    $stmt->bindColumn('address' , $address);
    $stmt->bindColumn('carid' , $carid);
    $stmt->bindColumn('number' , $number);
    $stmt->bindColumn('telegram' , $telegram);
    $stmt->bindColumn('instagram' , $instagram);
    $stmt->bindColumn('socialid' , $socialid);
    $stmt->bindColumn('extra' , $extra);
        echo "<table><caption>موارد اخیرا افزوده شده </caption>
    <tr>
    <th></th>
    <th > تصویر </th>
    <th > نام </th>
<th> نام خانوادگی </th>
<th> تاریخ تولد </th>
<th> شغل </th>
<th> رتبه سازمانی </th>
<th> شهر </th>
<th> زبان/لهجه </th>
<th> آدرس </th>
<th> پلاک خودرو </th>
<th> تلفن/موبایل </th>
<th> Id تلگرام </th>
<th> Id اینستاگرام </th>
<th> Idهای دیگر </th>
<th> توضیحات </th>
    </tr>";
        $count=0;
        while ($stmt->fetch(PDO::FETCH_BOUND)){
            foreach (array("name","family","birthdate","job","jobtitle","city","lang","address","carid","number","extra") as $fa_item){
                $$fa_item=num_to_fa($$fa_item);
            }
            $count++;
    echo "<tr>
        <td>".num_to_fa($count)."</td>
<td> <img id='img' src='./$faces_dir/$img' /></td>
<td> $name </td>
<td> $family </td>
<td> $birthdate </td>
<td> $job </td>
<td> $jobtitle </td>
<td> $city </td>
<td> $lang </td>
<td> $address </td>
<td> $carid </td>
<td> $number </td>
<td> $telegram </td>
<td> $instagram </td>
<td> $socialid </td>
<td> $extra </td>
          </tr>"  ;
    }
          ?>
      </table></div>
      <div id="note" class="fa_text">
          <ul>
              <li>همیشه به یاد داشته باشید که شما مقصر نیستید و چیزهایی مثل خندیدن، لباس پوشیدن، زیبا بودن، حرف زدن، تنهایی راه رفتن، اعتماد کردن و... دلیلی برای آزاردیدن نیست.</li>
              <li>آزارگر از خود مردم است و همانطوری که شاخ و دم نداره همونطوری هم ممکنه سیبیل یا ریش داشته باشه یا نداشته باشه یا شغل داشته باشه یا نه، 
              می تونه دکتر و وزیر و کیل باشه یا نباشه می تونه فامیل و آشنا باشه یا نباشه    </li>
              <li>آزارگر ممکنه در نگاه اول فرد متشخص و با احترامی به نظر بیاد
              پس اگر مورد آزار قرارگرفتید باخودتون صادق باشید و رفتار آزارگر رو با حرفهایی مثل« از این فردبعیده» «شاید من خیلی سخت می گیرم»و... توجیه نکنید. آزار، آزار است حتی اگر توسط معشوق تان صورت بگیرد. </li>
              <li>متاسفانه آزار در جامعه و حتی در خانه بسیار زیاد است.ولی این رایج بودن دلیلی بر حقانیت آزار نیست.</li> 
              <li>اطلاعات این سایت توسط داوطلبان و آزاردیدگان تکمیل می شود و احتمال واردکردن اطلاعات غرض ورزانه وجود دارد  </li>
              <li>این سایت جهت جمع آوری اطلاعات مربوط به آزارگران جنسی زنان می باشد هدف این سایت در راستای کاهش  خشونت علیه زنان و مسئولیت پذیری آزارگر از طریق افشا سازی  آزارگران است.</li>
              <li>به دلیل پاسداشت حریم شخصی،آزادی فردی و حفاظت از کاربران هیچ اطلاعات قابل ردگیری(مثل آی پی، صفحه های بازدید شده،عبارات مورد جست وجو و... ) از بازدیدکنندگان یا ثبت کنندگان ذخیره نمی شود. </li>
              <li>   آدرس های جایگزین ما در شبکه های دارک وب  رایج : (این آدرس ها کاملا ایمن و غیرقابل سانسور هستن و به مراتب از سایت اصلی فعالیت کاربران را ناشناس تر  نگه می دارند)
                  <ul class="link">
              <li> tor onion addreess </li>
              <li> zeronet</li>
              <li> I2P </li>
              <li>freenet</li>
              <li>gnunet</li>
                  </ul>
              </li>
              
              
          </ul>
      </div>
<?php echo $html_footer; ?>
